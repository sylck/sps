# Acknowledgements

This document provides a list of software and sound works used in this
project. Modified works have been denoted as such by providing a list of
modifications in the *Modifications* field. Original works, used as provided
by the author, do not have such a field. License notices found in used
packages have been provided in this document's [*License
notices*](#license-notices) section for instances when it was not preserved as
part of project structure.

## Works used

Path in project:
[`Assets/Audio/AmbientLoop01.wav`](Assets/Audio/AmbientLoop01.wav) <br>
Title: [Swamp Environment
Audio](https://opengameart.org/content/swamp-environment-audio) <br>
Author: [LokiF](https://opengameart.org/users/lokif) <br>
License: [CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/) <br>
Package name: `swamp_sounds.zip` <br>
Path in package: `atmosphere_1.ogg` <br>
Modifications: converted from Ogg Vorbis format to WAV

---

Path in project:
[`Assets/Audio/AmbientLoop02.wav`](Assets/Audio/AmbientLoop02.wav) <br>
Title: [Swamp Environment
Audio](https://opengameart.org/content/swamp-environment-audio) <br>
Author: [LokiF](https://opengameart.org/users/lokif) <br>
License: [CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/) <br>
Package name: `swamp_sounds.zip` <br>
Path in package: `atmosphere_2.ogg` <br>
Modifications: converted from Ogg Vorbis format to WAV

---

Path in project:
[`Assets/Audio/CylinderWhiteDeath.wav`](Assets/Audio/CylinderWhiteDeath.wav)
<br>
Title: [100 CC0 SFX #2](https://opengameart.org/content/100-cc0-sfx-2) <br>
Author: [rubberduck](https://opengameart.org/users/rubberduck) <br>
License: [CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/) <br>
Package name: `sfx_100_v2.zip` <br>
Path in package: `sfx100v2_air_01.ogg` <br>
Modifications: converted from Ogg Vorbis format to WAV, length cut, pitch
modified, volume reduced, mixed down to mono

---

Path in project:
[`Assets/Audio/CylinderWhiteSpawn.wav`](Assets/Audio/CylinderWhiteSpawn.wav)
<br>
Title: [100 CC0 SFX #2](https://opengameart.org/content/100-cc0-sfx-2) <br>
Author: [rubberduck](https://opengameart.org/users/rubberduck) <br>
License: [CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/) <br>
Package name: `sfx_100_v2.zip` <br>
Path in package: `sfx100v2_air_01.ogg` <br>
Modifications: converted from Ogg Vorbis format to WAV, length cut, pitch
modified, volume reduced, reversed, mixed down to mono

---

Path in project: [`Assets/Audio/Jump.wav`](Assets/Audio/Jump.wav) <br>
Title: [Rub Blanket](https://freesound.org/people/Nicolas4677/sounds/446574/)
<br>
Author: [Nicolas4677](https://freesound.org/people/Nicolas4677/) <br>
License: [CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/) <br>
Modifications: length cut, pitch modified

---

Path in project: [`Assets/Audio/Launch.wav`](Assets/Audio/Launch.wav) <br>
Title:
[fast-swing-air-woosh](https://freesound.org/people/CosmicEmbers/sounds/160756/)
<br>
Author: [Cosmic Embers](https://www.youtube.com/user/CosmicEmbers) <br>
License: [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/) <br>
Modifications: background noise removed

---

Path in project:
[`Assets/Audio/PlayerDamage.wav`](Assets/Audio/PlayerDamage.wav) <br>
Title: [UI Sound Effects
Library](https://opengameart.org/content/ui-sound-effects-library) <br>
Author: [Little Robot Sound
Factory](https://opengameart.org/users/little-robot-sound-factory)
(`www.littlerobotsoundfactory.com`) <br>
License: [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/) <br>
Package name: `UI Sound Library.zip` <br>
Path in package: `Wav/Click_Electronic/Click_Electronic_10.wav` <br>
Modifications: volume reduced <br>

Note: domain name `littlerobotsoundfactory.com` has expired; to respect
author's request for attribution the domain name has been specified, but has
not been made a link.

---

Path in project: [`Assets/Audio/PlayerHeal.wav`](Assets/Audio/PlayerHeal.wav)
<br>
Title: [Stunt Rally
sounds](https://opengameart.org/content/stunt-rally-sounds) <br>
Author: [Crystal Hammer](https://github.com/cryham), posted on OpenGameArt by
[Calinou](https://opengameart.org/users/calinou) <br>
License: [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/) <br>
Package name: `stunt-rally-sounds.zip` <br>
Path in package: `sounds/hud/lap.wav` <br>
Modifications: length cut, pitch and volume modified

Note: package contains license notices which have been provided in the
[*License notices*](#license-notices) section below.

---

Path in project: [`Assets/Audio/PlayerWin.wav`](Assets/Audio/PlayerWin.wav)
<br>
Title: [Stunt Rally
sounds](https://opengameart.org/content/stunt-rally-sounds) <br>
Author: [Crystal Hammer](https://github.com/cryham), posted on OpenGameArt by
[Calinou](https://opengameart.org/users/calinou) <br>
License: [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/) <br>
Package name: `stunt-rally-sounds.zip` <br>
Path in package: `sounds/hud/lap_best.wav` <br>
Modifications: volume reduced

Note: package contains license notices which have been provided in the
[*License notices*](#license-notices) section below.

---

Path in project:
[`Assets/Audio/ProjectileExplosion.wav`](Assets/Audio/ProjectileExplosion.wav)
<br>
Title: [Punches, hits, swords and
squishes.](https://opengameart.org/content/punches-hits-swords-and-squishes)
<br>
Author: Philippe Groarke
([tarfmagougou](https://opengameart.org/users/tarfmagougou)) <br>
License: [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/) <br>
Package name: `Socapex - Evol Online SFX - Punches and hits.zip` <br>
Path in package: `Socapex - Evol Online SFX - Punches and hits/Socapex -
new_hits_2.wav` <br>

Note: package contains license notice which has been provided in the
[*License notices*](#license-notices) section below.

---

Path in project: [`Assets/Audio/SlideLoop.wav`](Assets/Audio/SlideLoop.wav)
<br>
Title: [Rub Blanket](https://freesound.org/people/Nicolas4677/sounds/446574/)
<br>
Author: [Nicolas4677](https://freesound.org/people/Nicolas4677/) <br>
License: [CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/) <br>
Modifications: sped up, volume reduced

---

Path in project:
[`Assets/Audio/SpherePurpleExplosion.wav`](Assets/Audio/SpherePurpleExplosion.wav)
<br>
Title: [80 CC0 RPG SFX](https://opengameart.org/content/80-cc0-rpg-sfx) <br>
Author: [rubberduck](https://opengameart.org/users/rubberduck) <br>
License: [CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/) <br>
Package name: `80-CC0-RPG-SFX.zip` <br>
Path in package: `stones_01.ogg` <br>
Modifications: converted from Ogg Vorbis format to WAV, mixed down to mono

---

Path in project:
[`Assets/Audio/Inventory/Fail.wav`](Assets/Audio/Inventory/Fail.wav) <br>
Title: [RPG Sound Pack](https://opengameart.org/content/rpg-sound-pack) <br>
Author: Ben ([artisticdude](https://opengameart.org/users/artisticdude)) <br>
License: [CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/) <br>
Package name: `rpg_sound_pack.zip` <br>
Path in package: `RPG Sound Pack/interface/interface6.wav` <br>
Modifications: volume reduced

---

Path in project:
[`Assets/Audio/Inventory/Pick.wav`](Assets/Audio/Inventory/Pick.wav) <br>
Title: [3 Melee sounds](https://opengameart.org/content/3-melee-sounds) <br>
Author: [remaxim](https://opengameart.org/users/remaxim) <br>
License: [CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/) <br>
Package name: `melee sounds.zip` <br>
Path in package: `melee sounds/animal melee sound.wav` <br>
Modifications: length cut, pitch and volume modified, reversed <br>

Note: package contains license notice which has been provided in the
[*License notices*](#license-notices) section below.

---

Path in project:
[`Assets/Audio/Inventory/UI/Destroy.wav`](Assets/Audio/Inventory/UI/Destroy.wav)
<br>
Title: [202 More Sound
Effects](https://opengameart.org/content/202-more-sound-effects) <br>
Author: [OwlishMedia](https://opengameart.org/users/owlishmedia) <br>
License: [CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/) <br>
Package name: `MoreSounds.zip` <br>
Path in package: `Plastic/Plastic_19.wav` <br>
Modifications: length cut, volume modified

---

Path in project:
[`Assets/Audio/Inventory/UI/PickUp.wav`](Assets/Audio/Inventory/UI/PickUp.wav)
<br>
Title: [80 CC0 RPG SFX](https://opengameart.org/content/80-cc0-rpg-sfx) <br>
Author: [rubberduck](https://opengameart.org/users/rubberduck) <br>
License: [CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/) <br>
Package name: `80-CC0-RPG-SFX.zip` <br>
Path in package: `creature_misc_07.ogg` <br>
Modifications: converted from Ogg Vorbis format to WAV, volume reduced

---

Path in project:
[`Assets/Audio/Inventory/UI/PutDown.wav`](Assets/Audio/Inventory/UI/PutDown.wav)
<br>
Title: [80 CC0 RPG SFX](https://opengameart.org/content/80-cc0-rpg-sfx) <br>
Author: [rubberduck](https://opengameart.org/users/rubberduck) <br>
License: [CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/) <br>
Package name: `80-CC0-RPG-SFX.zip` <br>
Path in package: `creature_monster_01.ogg` <br>
Modifications: converted from Ogg Vorbis format to WAV, volume reduced

---

Path in project:
[`Assets/Audio/Inventory/UI/Split.wav`](Assets/Audio/Inventory/UI/Split.wav)
<br>
Title: [100 CC0 SFX #2](https://opengameart.org/content/100-cc0-sfx-2) <br>
Author: [rubberduck](https://opengameart.org/users/rubberduck) <br>
License: [CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/) <br>
Package name: `sfx_100_v2.zip` <br>
Path in package: `sfx100v2_misc_09.ogg` <br>
Modifications: converted from Ogg Vorbis format to WAV, volume reduced

---

Path in project:
[`Assets/Audio/UI/ButtonClick.wav`](Assets/Audio/UI/ButtonClick.wav) <br>
Title: [100 CC0 SFX #2](https://opengameart.org/content/100-cc0-sfx-2) <br>
Author: [rubberduck](https://opengameart.org/users/rubberduck) <br>
License: [CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/) <br>
Package name: `sfx_100_v2.zip` <br>
Path in package: `sfx100v2_misc_07.ogg` <br>
Modifications: converted from Ogg Vorbis format to WAV, length cut, volume
modified

---

Path in project:
[`Assets/Audio/UI/ButtonHover.wav`](Assets/Audio/UI/ButtonHover.wav) <br>
Title: [Fantasy Sound Effects (Tinysized
SFX)](https://opengameart.org/content/fantasy-sound-effects-tinysized-sfx) <br>
Author: Jan Schupke ([Vehicle](https://opengameart.org/users/vehicle)) <br>
License: [CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/) <br>
Package name: `tinysized.zip` <br>
Path in package: `sfx-cc0/water-drop-01.wav` <br>
Modifications: length cut, volume modified

---

Path in project: [`Assets/External
Assets/NavMeshComponents`](Assets/External%20Assets/NavMeshComponents) <br>
Title: [Components for Runtime NavMesh
Building](https://github.com/Unity-Technologies/NavMeshComponents) <br>
Author: Unity Technologies <br>
License: [MIT](https://opensource.org/licenses/MIT), a copy located at
[`Assets/External
Assets/NavMeshComponents/LICENSE`](Assets/External%20Assets/NavMeshComponents/LICENSE)
as well

## License notices

### [3 Melee sounds](https://opengameart.org/content/3-melee-sounds)

Author: [remaxim](https://opengameart.org/users/remaxim) <br>
License: [CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/) <br>
Package name: `melee sounds.zip`

Path in package: `melee sounds/melee.txt`
```
Original by qubodup/Iwan Gabovitch - qubodup.net - qubodup@gmail.com
Submitted to opengameart.org

Free for *any* use. This means that the files are in the public domain, Gemeindegut, cc0-licensed or WTFPL-licensed. Whatever you want.

Attribute me and/or opengameart if you can and want!
Edited by remaxim 
Submitted to opengameart.org

Free for *any* use. This means that the files are in the public domain, Gemeindegut, cc0-licensed or WTFPL-licensed. Whatever you want.

Attribute me and/or opengameart if you can and want!

Edited by remaxim 
Submitted to opengameart.org

Free for *any* use. This means that the files are in the public domain, Gemeindegut, cc0-licensed or WTFPL-licensed. Whatever you want.

Attribute me and/or opengameart if you can and want!
Edited by remaxim 
Submitted to opengameart.org

Free for *any* use. This means that the files are in the public domain, Gemeindegut, cc0-licensed or WTFPL-licensed. Whatever you want.

Attribute me and/or opengameart if you can and want!

```

### [Punches, hits, swords and squishes.](https://opengameart.org/content/punches-hits-swords-and-squishes)

Author: Philippe Groarke
([tarfmagougou](https://opengameart.org/users/tarfmagougou)) <br>
License: [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/) <br>
Package name: `Socapex - Evol Online SFX - Punches and hits.zip`

Path in package: `Socapex - Evol Online SFX - Punches and hits/LICENSES.txt`
```
afterguard - unionsword 1865                                (CC-BY 3.0  -- http://www.freesound.org/people/afterguard/sounds/44660/)
benboncan - bang                                            (CC-BY 3.0  -- http://www.freesound.org/people/Benboncan/sounds/63831/)
black-snow - sword slice 23                                 (CC-BY 3.0  -- http://www.freesound.org/people/Black%20Snow/sounds/109432/)
cgeffex - bang thunder                                      (CC-BY 3.0  -- http://www.freesound.org/people/CGEffex/sounds/121566/)
cgeffex - swishes                                           (CC-BY 3.0  -- http://www.freesound.org/people/CGEffex/sounds/93081/)
cinevid - breakingneck                                      (CC0        -- http://www.freesound.org/people/cinevid/sounds/144159/)
cosmicembers - cape swoosh                                  (CC-BY 3.0  -- http://www.freesound.org/people/CosmicEmbers/sounds/161415/)
cosmicembers - fast swing air woosh                         (CC-BY 3.0  -- http://www.freesound.org/people/CosmicEmbers/sounds/160756/)
djahren - plastic bang                                      (CC-BY 3.0  -- http://www.freesound.org/people/djahren/sounds/84803/)
dobroide - 20061030 bang ms                                 (CC-BY 3.0  -- http://www.freesound.org/people/dobroide/sounds/24633/)
erdie - egg01                                               (CC-BY 3.0  -- http://www.freesound.org/people/Erdie/sounds/155873/)
erdie - sword04                                             (CC-BY 3.0  -- http://www.freesound.org/people/Erdie/sounds/27858/)
filipe chagas - lemon juicy squeeze fruit                   (CC0        -- http://www.freesound.org/people/Filipe%20Chagas/sounds/91915/)
herbertboland - distantcrackcountryside                     (CC-BY 3.0  -- http://www.freesound.org/people/HerbertBoland/sounds/142020/)
iedlabs - cruch eggshells medium                            (CC-BY 3.0  -- http://www.freesound.org/people/IEDlabs/sounds/82318/)
jobro - sword pulled 2                                      (CC-BY 3.0  -- http://www.freesound.org/people/jobro/sounds/74832/)
joelaudio - lettuce snap crunch long 002                    (CC-BY 3.0  -- http://www.freesound.org/people/JoelAudio/sounds/135462/)
jymdavis - mud squish take 2                                (CC-BY 3.0  -- http://www.freesound.org/people/jymdavis/sounds/140478/)
kibibu - sword_4                                            (CC0        -- http://www.freesound.org/people/kibibu/sounds/22428/)
kleanthism - kleaudio_crackingbone2_msST                    (CC0        -- http://www.freesound.org/people/kleanthism/sounds/145363/)
lolamadeus - watermelon squelch4                            (CC0        -- http://www.freesound.org/people/lolamadeus/sounds/159669/)
muses212 - stirring pasta batch1 bip                        (CC0        -- http://www.freesound.org/people/muses212/sounds/77251/)
qat - sheath sword                                          (CC0        -- http://www.freesound.org/people/Qat/sounds/107590/)
qat - unsheath sword                                        (CC0        -- http://www.freesound.org/people/Qat/sounds/107589/)
qubodup - punching bag                                      (CC0        -- http://www.freesound.org/people/qubodup/sounds/53985/)
qubodup - swosh swoosh whoosh air sound free high quality   (CC0        -- http://www.freesound.org/people/qubodup/sounds/60026/)
qubodup - swosh sword swing                                 (CC0        -- http://www.freesound.org/people/qubodup/sounds/59992/)
robkinsons - sworddraw                                      (CC-BY 3.0  -- http://www.freesound.org/people/Robkinsons/sounds/103128/)
slave2thelight - blood and gore fx performance 2            (CC-BY 3.0  -- http://www.freesound.org/people/Slave2theLight/sounds/157113/)
snowflakes - whips01                                        (CC0        -- http://www.freesound.org/people/snowflakes/sounds/72192/)
stereostereo - 05 broken hand foley                         (CC-BY 3.0  -- http://www.freesound.org/people/stereostereo/sounds/117342/)
velvorn - punches                                           (CC0        -- http://www.freesound.org/people/Velvorn/sounds/96486/)
xenognosis - egg pulp                                       (CC0        -- http://www.freesound.org/people/xenognosis/sounds/137254/)
ztrees1 - punch                                             (CC-BY 3.0  -- http://www.freesound.org/people/ztrees1/sounds/134934/)

```

### [Stunt Rally sounds](https://opengameart.org/content/stunt-rally-sounds)

Author: [Crystal Hammer](https://github.com/cryham), posted on OpenGameArt by
[Calinou](https://opengameart.org/users/calinou) <br>
License: [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/) <br>
Package name: `stunt-rally-sounds.zip`

Path in package: `sounds/_sounds.txt`
```
-- original VDrift sounds

bump_front
bump_rear
grass
gravel
tire_squeal
wind

-- new sounds from
http://www.freesound.org/

  water,mud:
cgeffex  large splashes

  mud_cont, water_cont
justinbw  water spigget onto mud 1
justinbw  water spigget onto mud 2

  terrain,dirt:
halleck  hit with dirt spray 1
halleck  hit with dirt spray 1 body
halleck  hit with dirt spray 2 body
halleck  hit with dirt spray 3

  boost
nathanshadow  thruster level ii

```

Path in package: `sounds/crash/_crash.txt`
```

**  01..12.wav
crash hits, made from smaller cuts:

-- Author: halleck
-- License:  CC BY SA 3.0

metal medium hit
http://www.freesound.org/people/Halleck/sounds/121668/

metal hits medium 1
http://www.freesound.org/people/Halleck/sounds/121665/

metal hits light 1
http://www.freesound.org/people/Halleck/sounds/121664/

metal thump
http://www.freesound.org/people/Halleck/sounds/121685/

big metal side impact 1
http://www.freesound.org/people/Halleck/sounds/121621/

big metal side impact 2
http://www.freesound.org/people/Halleck/sounds/121622/

metal crash 1
http://www.freesound.org/people/Halleck/sounds/121655/

**  scrap.wav
metal roll cage hits heavy
http://www.freesound.org/people/Halleck/sounds/121669/

**  screech.wav
metal screech and scraping
http://www.freesound.org/people/Halleck/sounds/121677/


-- Author: sagetyrtle
-- License:  CC 0

crash
http://www.freesound.org/people/sagetyrtle/sounds/40158/

```
