# Some Polygons Suck

A first person shooter game developed using Unity. You are a lone cube trapped
in a world of cylinders. Fight your way to glory by shooting pesky cylinders
out of existence and collecting all the cubes. Collect spheres and find out
their purpose ... or, for some of them, the lack of it.

## Installation

Download the [latest release](https://gitlab.com/sylck/sps/-/releases), unpack
it and run `SomePolygonsSuck.exe`.

## Gameplay

<img src="Images/Picking.gif" width="320" title="Picking"/>&nbsp;
<img src="Images/InventoryManagement.gif" width="320"
title="Inventory management"/>&nbsp;
<img src="Images/Victory.gif" width="320" title="Victory"/>&nbsp;
<img src="Images/Death.gif" width="320" title="Death"/>

## Credits

Some Polygons Suck uses software and sound works created by the following
parties: <br>
Ben ([artisticdude](https://opengameart.org/users/artisticdude)), [Cosmic
Embers](https://www.youtube.com/user/CosmicEmbers), [Crystal
Hammer](https://github.com/cryham), Jan Schupke
([Vehicle](https://opengameart.org/users/vehicle)), [Little Robot Sound
Factory](https://opengameart.org/users/little-robot-sound-factory),
[LokiF](https://opengameart.org/users/lokif),
[Nicolas4677](https://freesound.org/people/Nicolas4677/),
[OwlishMedia](https://opengameart.org/users/owlishmedia), Philippe Groarke
([tarfmagougou](https://opengameart.org/users/tarfmagougou)),
[remaxim](https://opengameart.org/users/remaxim),
[rubberduck](https://opengameart.org/users/rubberduck) and Unity Technologies.

For more details, see [Acknowledgements](ACKNOWLEDGEMENTS.md).
