﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameManagerEvents
{
    public const string GameWillStartRunning = "gameManager.gameWillStartRunning";
    public const string GameWillStopRunning = "gameManager.gameWillStopRunning";

    public const string EnteredVictoryState = "gameManager.enteredVictoryState";
    public const string EnteredDeathState = "gameManager.enteredDeathState";
}

public class GameManager : MonoBehaviour
{
    private bool m_Running;
    public bool running { get { return m_Running; } }

    private bool m_EnteredFinalState;
    public bool enteredFinalState { get { return m_EnteredFinalState; } }

    private void OnEnable()
    {
        Subscribe();
    }

    private void OnDisable()
    {
        Unsubscribe();
    }

    private void Subscribe()
    {
        EventManager.AddListener(
            UI.MenuEvents.Showing,
            StopRunningHandler);
        EventManager.AddListener(
            UI.MenuEvents.Hidden,
            StartRunningHandler);
        EventManager.AddListener(
            UI.Loading.ScreenEvents.Hidden,
            StartRunningHandler);
        EventManager.AddListener(
            EventUtility.TypedEvent(HealthEvents.ZeroHpReached, ThingType.Player),
            OnPlayerZeroHpReached);
    }

    private void Unsubscribe()
    {
        EventManager.RemoveListener(
            UI.MenuEvents.Showing,
            StopRunningHandler);
        EventManager.RemoveListener(
            UI.MenuEvents.Hidden,
            StartRunningHandler);
        EventManager.RemoveListener(
            UI.Loading.ScreenEvents.Hidden,
            StartRunningHandler);
        EventManager.RemoveListener(
            EventUtility.TypedEvent(HealthEvents.ZeroHpReached, ThingType.Player),
            OnPlayerZeroHpReached);
    }

    private void OnPlayerZeroHpReached(EventData data)
    {
        TrySetDeathState();
    }

    private void StopRunningHandler(EventData data)
    {
        StopRunning();
    }

    private void StartRunningHandler(EventData data)
    {
        StartRunning();
    }

    public void TrySetDeathState()
    {
        if (TryEnterFinalState())
            EventManager.Invoke(GameManagerEvents.EnteredDeathState);
    }

    public void TrySetVictoryState()
    {
        if (TryEnterFinalState())
            EventManager.Invoke(GameManagerEvents.EnteredVictoryState);
    }

    private bool TryEnterFinalState()
    {
        // do nothing if:
        // * game currently paused 
        //  or 
        // * has already reached a final state, be it death or victory
        if (!m_Running || m_EnteredFinalState)
            return false;

        m_EnteredFinalState = true;
        Unsubscribe();

        return true;
    }

    private void StopRunning()
    {
        EventManager.Invoke(GameManagerEvents.GameWillStopRunning);
        Time.timeScale = GameConstants.TimeScale.Stopped;

        m_Running = false;
    }

    private void StartRunning()
    {
        EventManager.Invoke(GameManagerEvents.GameWillStartRunning);
        Time.timeScale = GameConstants.TimeScale.Realtime;

        m_Running = true;
    }
}
