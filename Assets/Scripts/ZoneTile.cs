﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoneTile : MonoBehaviour
{
    public enum TileType
    {
        Damaging,
        Healing
    }

    [SerializeField]
    private ZoneManager zoneManager;

    [SerializeField]
    private TileType tileType;

    private void OnTriggerEnter(Collider other)
    {
        zoneManager.ZoneEnter(other.gameObject, tileType);
    }

    private void OnTriggerStay(Collider other)
    {
        zoneManager.ZoneStay(other.gameObject, tileType);
    }

    private void OnTriggerExit(Collider other)
    {
        zoneManager.ZoneExit(other.gameObject, tileType);
    }
}
