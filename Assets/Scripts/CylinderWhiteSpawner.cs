﻿using System.Collections.Generic;
using UnityEngine;

public class CylinderWhiteSpawner : MonoBehaviour
{
    [SerializeField]
    private Transform spawner;
    
    [SerializeField]
    private Transform[] patrolPoints;

    [SerializeField]
    private CylinderWhiteMovement cylinderWhitePrefab;

    [SerializeField]
    private float spawnYPosition;

    [SerializeField]
    private float respawnInterval;

    private Vector3[] patrolPointPositions;
    private Timer respawnTimer;

    private void Start()
    {
        // respawn timer
        respawnTimer = new Timer.Builder()
            .SetInterval(respawnInterval)
            .SetAutoReset(false)
            .AddElapsedListener(InstantiateCylinderWhite)
            .Build();

        // patrol points cleanup
        List<Vector3> patrolPointPositionsList = new List<Vector3>();

        for (int i = 0; i < patrolPoints.Length; i++) 
        {
            Transform patrolPoint = patrolPoints[i];

            if (patrolPoint == null)
                continue;

            patrolPointPositionsList.Add(patrolPoint.position);
        }

        patrolPointPositions = patrolPointPositionsList.ToArray();

        if (patrolPointPositions.Length == 0)
        {
            Debug.Log(gameObject.name + " has no patrol points assigned, spawned CylinderWhite " + 
                "will be stuck at its initial position.");
        }

        InstantiateCylinderWhite();
    }

    private void Update()
    {
        respawnTimer.Tick(Time.deltaTime);
    }

    private void InstantiateCylinderWhite()
    {
        Vector3 spawnPosition = spawner.position;
        spawnPosition.y = spawnYPosition;

        CylinderWhiteMovement movement = Instantiate(cylinderWhitePrefab, spawnPosition, 
            Quaternion.identity, spawner);
        movement.SetPatrolPoints(patrolPointPositions);

        CylinderWhiteStateManager stateManager = movement.GetComponent<CylinderWhiteStateManager>();
        stateManager.dead += OnDead;
    }

    private void OnDead()
    {
        respawnTimer.Start();
    }
}
