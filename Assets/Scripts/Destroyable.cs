﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Destroyable : MonoBehaviour
{
    [SerializeField]
    private Health health;

    private void OnEnable()
    {
        health.zeroHpReached += HandleDestruction;
    }

    private void OnDisable()
    {
        health.zeroHpReached -= HandleDestruction;
    }

    protected abstract void HandleDestruction();

    protected void DestroySelf()
    {
        Destroy(gameObject);
    }
}
