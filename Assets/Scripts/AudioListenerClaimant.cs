﻿using UnityEngine;

public class AudioListenerClaimant : MonoBehaviour
{
    [SerializeField]
    private AudioListener audioListener;

    // Start is called before the first frame update
    void Start()
    {
        FindObjectOfType<AudioListenerManager>().Claim(audioListener);
    }
}
