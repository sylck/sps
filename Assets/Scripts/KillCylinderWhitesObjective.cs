﻿public class KillCylinderWhitesObjective : Objective
{
    private int cylinderWhiteCount;

    private void OnEnable()
    {
        EventManager.AddListener(
            EventUtility.TypedEvent(ThingEvents.Started, ThingType.CylinderWhite), 
            OnCylinderWhiteStarted);
        EventManager.AddListener(
            EventUtility.TypedEvent(HealthEvents.ZeroHpReached, ThingType.CylinderWhite), 
            OnCylinderWhiteZeroHpReached);
    }

    private void OnDisable()
    {
        EventManager.RemoveListener(
            EventUtility.TypedEvent(ThingEvents.Started, ThingType.CylinderWhite),
            OnCylinderWhiteStarted);
        EventManager.RemoveListener(
            EventUtility.TypedEvent(HealthEvents.ZeroHpReached, ThingType.CylinderWhite),
            OnCylinderWhiteZeroHpReached);
    }

    private void OnCylinderWhiteStarted(EventData data)
    {
        cylinderWhiteCount++;
    }

    private void OnCylinderWhiteZeroHpReached(EventData data)
    {
        cylinderWhiteCount--;

        TryRaiseCompleted();
    }

    public override bool IsCompleted()
    {
        return cylinderWhiteCount == 0;
    }
}
