﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShooting : MonoBehaviour
{
    [SerializeField]
    private float cooldownInterval = .2f,
                  projectileInitialForwardOffset = .2f,
                  projectileInitialUpOffset = -.4f,
                  projectileTargetDistance = 3.25f,
                  projectileVelocityMagnitude = 20f;
    
    [SerializeField]
    private Transform player,
                      playerCamera;

    [SerializeField]
    private RigidlikeController playerController;

    [SerializeField]
    private PoolMember projectilePrefab;

    [SerializeField]
    private AudioSource audioSource;

    [SerializeField]
    private AudioClip launchSound, 
                      noAmmoSound;

    [SerializeField]
    private PlayerInventory inventory;

    private Timer cooldownTimer;

    private const float k_GroundLevelY = .2f;

    private void Awake()
    {
        cooldownTimer = new Timer.Builder()
            .SetInterval(cooldownInterval)
            .SetAutoReset(false)
            .Build();
    }

    // Update is called once per frame
    void Update()
    {
        cooldownTimer.Tick(Time.deltaTime);

        TryFire();
    }

    private void TryFire()
    {
        if (!GetFireInput() || cooldownTimer.enabled)
            return;
        
        if (TakeFromInventory())
            Fire();
        else
            PlayNoAmmoSound();

        // start cooldownTimer even if no ammo found to prevent no ammo sound repeating too 
        // frequently
        cooldownTimer.Start();
    }

    private bool GetFireInput()
    {
        return Input.GetMouseButton(0);
    }

    private bool TakeFromInventory()
    {
        return inventory.RemoveItem(ThingType.SphereOrange);
    }

    private void PlayNoAmmoSound()
    {
        audioSource.PlayOneShot(noAmmoSound);
    }

    private void Fire()
    {
        LaunchProjectile();
        PlayLaunchSound();
    }

    private void PlayLaunchSound()
    {
        audioSource.PlayOneShot(launchSound);
    }

    private void LaunchProjectile()
    {
        GameObject projectile = projectilePrefab.GetInstance();
        Rigidbody projectileRigidbody = projectile.GetComponent<Rigidbody>();
  
        // init position
        Vector3 initialPosition = CalculateProjectileInitialPosition();
        projectile.transform.position = initialPosition;

        // init velocity        
        Vector3 targetPosition = CalculateProjectileTargetPosition();       
        Vector3 direction = (targetPosition - initialPosition).normalized;
        projectileRigidbody.velocity = direction * projectileVelocityMagnitude;
    }

    private Vector3 CalculateProjectileInitialPosition()
    {
        Vector3 cameraPosition = playerCamera.position,
                cameraForward = playerCamera.forward,
                cameraUp = playerCamera.up;
        Vector3 playerPosition = player.position,
                playerForward = player.forward,
                playerUp = player.up;

        // absolute position vectors depend on velocity, relative direction vectors (forward, up) 
        // don't - they depend only on mesh and camera angles
        cameraPosition = playerController.CompensateForMovement(cameraPosition);
        playerPosition = playerController.CompensateForMovement(playerPosition);

        Vector3 initialPosition = cameraPosition +
            projectileInitialForwardOffset * cameraForward +
            projectileInitialUpOffset * cameraUp;

        // ensure initialPosition is at least as far from player mesh as it would be when 
        // camera's angle is zero
        Vector3 zeroAnglePosition = cameraPosition +
            projectileInitialForwardOffset * playerForward +
            projectileInitialUpOffset * playerUp;

        Vector3 ipMeshXzDistance = initialPosition - playerPosition;
        ipMeshXzDistance.y = 0f;
        Vector3 zapMeshXzDistance = zeroAnglePosition - playerPosition;
        zapMeshXzDistance.y = 0f;

        if (ipMeshXzDistance.sqrMagnitude >= zapMeshXzDistance.sqrMagnitude)
        {
            // no changes needed, good to go
            return initialPosition;
        }

        // changes are needed, initialPosition is within player mesh
        Vector3 xzDelta = ipMeshXzDistance - zapMeshXzDistance;

        Vector3 cameraForwardXz = cameraForward;
        cameraForwardXz.y = 0f;

        float forwardCorrectionFactor = xzDelta.magnitude / cameraForwardXz.magnitude;

        initialPosition += forwardCorrectionFactor * cameraForward;

        // if corrected initialPosition is going to be below ground, reset Y to groundLevelY
        if (initialPosition.y < k_GroundLevelY)
        {
            initialPosition.y = k_GroundLevelY;
        }

        return initialPosition;
    }

    private Vector3 CalculateProjectileTargetPosition()
    {
        Vector3 cameraPosition = playerCamera.position,
                cameraForward = playerCamera.forward;

        cameraPosition = playerController.CompensateForMovement(cameraPosition);

        return cameraPosition +
            projectileTargetDistance * cameraForward;
    }
}
