﻿using UnityEngine;

public class PlayerHealthSound : MonoBehaviour
{
    [SerializeField]
    private Health health;

    [SerializeField]
    private AudioSource audioSource;

    [SerializeField]
    private AudioClip damageSound,
                      healSound;

    private void OnEnable()
    {
        health.hpDecreased += OnHpDecreased;
        health.hpIncreased += OnHpIncreased;
    }

    private void OnDisable()
    {
        health.hpDecreased -= OnHpDecreased;
        health.hpIncreased -= OnHpIncreased;
    }

    private void OnHpDecreased(int hpDelta)
    {
        PlayDamageSound();
    }

    private void PlayDamageSound()
    {
        audioSource.PlayOneShot(damageSound);
    }

    private void OnHpIncreased(int hpDelta)
    {
        PlayHealSound();
    }

    private void PlayHealSound()
    {
        audioSource.PlayOneShot(healSound);
    }
}
