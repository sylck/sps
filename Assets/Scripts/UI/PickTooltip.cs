﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class PickTooltip : MonoBehaviour
    {
        [SerializeField]
        private GameObject container;

        [SerializeField]
        private Text text;

        private bool pickableActive;

        private void OnEnable()
        {
            EventManager.AddListener(PickableEvents.Activated, OnPickableActivated);
            EventManager.AddListener(PickableEvents.Picking, OnPickablePicking);
            EventManager.AddListener(PickableEvents.Deactivated, OnPickableDeactivated);

            EventManager.AddListener(DialogEvents.Opening, OnDialogOpening);
            EventManager.AddListener(DialogEvents.Closed, OnDialogClosed);

            EventManager.AddListener(
                EventUtility.TypedEvent(
                    LoadManagerEvents.SceneUnloaded, SceneUtility.SceneType.Level),
                OnLevelUnloaded);
        }

        private void OnDisable()
        {
            EventManager.RemoveListener(PickableEvents.Activated, OnPickableActivated);
            EventManager.RemoveListener(PickableEvents.Picking, OnPickablePicking);
            EventManager.RemoveListener(PickableEvents.Deactivated, OnPickableDeactivated);

            EventManager.RemoveListener(DialogEvents.Opening, OnDialogOpening);
            EventManager.RemoveListener(DialogEvents.Closed, OnDialogClosed);

            EventManager.RemoveListener(
                EventUtility.TypedEvent(
                    LoadManagerEvents.SceneUnloaded, SceneUtility.SceneType.Level),
                OnLevelUnloaded);
        }

        private void OnPickableActivated(EventData data)
        {
            ItemData item = data.obj as ItemData;
               
            pickableActive = true;

            text.text = MakeMessage(item.displayName);
        
            GameObjectUtility.Show(container);
        }

        private string MakeMessage(string displayName)
        {
            return "<b>[E]</b> Pick up <b>" + displayName + "</b>";
        }

        private void OnPickablePicking(EventData data)
        {
            pickableActive = false;
        
            GameObjectUtility.Hide(container);
        }

        private void OnPickableDeactivated(EventData data)
        {
            pickableActive = false;

            GameObjectUtility.Hide(container);
        }

        private void OnDialogOpening(EventData data)
        {
            // don't hide if wasn't shown
            if (!pickableActive)
                return;

            GameObjectUtility.Hide(container);
        }

        private void OnDialogClosed(EventData data)
        {
            // don't reshow if player was not looking at a pickable as `text` might contain a 
            // message which is no longer accurate
            if (!pickableActive)
                return;

            GameObjectUtility.Show(container);
        }

        private void OnLevelUnloaded(EventData data)
        {
            pickableActive = false;

            GameObjectUtility.Hide(container);
        }
    }
}
