﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UI
{
    public static class MenuEvents
    {
        public const string Showing = "ui.menu.showing";
        public const string Hidden = "ui.menu.hidden";
    }

    public class Menu : MonoBehaviour, IDialog
    {
        [SerializeField]
        private LoadManager loadManager;
        
        [SerializeField]
        private GameObject menuContainer;

        [SerializeField]
        private GameObject titleContainer, deathStateTitleContainer, victoryStateTitleContainer;

        [SerializeField]
        private GameObject playButton, restartButton;

        private bool pueaFailed;
        private GameObject[] uiElements, mainMenuElements, pauseMenuElements, 
            deathStateElements, victoryStateElements;

        public enum Layout
        {
            MainMenu,
            PauseMenu,
            DeathState,
            VictoryState
        }

        public void OnPlay()
        {
            loadManager.LoadScene(SceneUtility.SceneType.Level);
        }

        public void OnRestart()
        {
            loadManager.ReloadActiveScene();
        }

        public void OnExit()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
        }

        private bool PopulateUiElementArrays()
        {
            uiElements = new GameObject[]
            {
                titleContainer, deathStateTitleContainer, victoryStateTitleContainer,
                playButton, restartButton
            };

            mainMenuElements = new GameObject[]
            {
                titleContainer,
                playButton
            };

            pauseMenuElements = new GameObject[]
            {
                titleContainer,
                restartButton
            };

            deathStateElements = new GameObject[]
            {
                deathStateTitleContainer,
                restartButton
            };

            victoryStateElements = new GameObject[]
            {
                victoryStateTitleContainer,
                restartButton
            };

            foreach (GameObject uiElement in uiElements)
            {
                if (!uiElement)
                {
                    gameObject.SetActive(false);
                    pueaFailed = true;
                    Debug.Log(
                        "One or more GameObjects have not been assigned. Menu GameObject " + 
                        "has been deactivated and further method calls will be ignored."
                    );
                    return false;
                }
            }

            return true;
        }

        public bool SetLayout(Layout layout)
        {
            if (pueaFailed || (uiElements == null && !PopulateUiElementArrays()))
                return false;

            GameObjectUtility.Hide(uiElements);

            switch (layout)
            {
                case Layout.MainMenu:
                    GameObjectUtility.Show(mainMenuElements);
                    break;
                case Layout.PauseMenu:
                    GameObjectUtility.Show(pauseMenuElements);
                    break;
                case Layout.DeathState:
                    GameObjectUtility.Show(deathStateElements);
                    break;
                case Layout.VictoryState:
                    GameObjectUtility.Show(victoryStateElements);
                    break;
            }

            return true;
        }

        public void Show()
        {
            if (pueaFailed)
                return;

            // don't show if already shown
            if (GameObjectUtility.Visible(menuContainer))
                return;

            EventManager.Invoke(MenuEvents.Showing);
            GameObjectUtility.Show(menuContainer);
        }

        public void Hide()
        {
            if (pueaFailed)
                return;

            if (!GameObjectUtility.Visible(menuContainer))
                return;

            GameObjectUtility.Hide(menuContainer);
            EventManager.Invoke(MenuEvents.Hidden);
        }
    }
}
