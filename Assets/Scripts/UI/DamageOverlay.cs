﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class DamageOverlay : MonoBehaviour
    {
        [SerializeField]
        private Image overlayImage;

        [SerializeField]
        private float minTintAlpha = .2f,
                      maxTintAlpha = .9f;

        [SerializeField]
        private float minTintTime = .5f,
                      maxTintTime = 4f;

        private float fadeStartAlpha;
        private Timer fadeTimer;

        private const float tintAlphaTransparent = 0f;

        private void Awake()
        {
            fadeTimer = new Timer.Builder()
                .SetAutoReset(false)
                .AddTickedListener(FadeTint)
                .Build();
        }

        private void OnEnable()
        {
            EventManager.AddListener(
                EventUtility.TypedEvent(
                    HealthEvents.HpDecreased, ThingType.Player),
                OnHpDecreased);
            EventManager.AddListener(
                EventUtility.TypedEvent(
                    LoadManagerEvents.SceneUnloaded, SceneUtility.SceneType.Level),
                OnLevelUnloaded);
        }

        // Update is called once per frame
        void Update() 
        {
            fadeTimer.Tick(Time.deltaTime);
        }

        private void OnDisable()
        {
            EventManager.RemoveListener(
                EventUtility.TypedEvent(
                    HealthEvents.HpDecreased, ThingType.Player),
                OnHpDecreased);
            EventManager.RemoveListener(
                EventUtility.TypedEvent(
                    LoadManagerEvents.SceneUnloaded, SceneUtility.SceneType.Level),
                OnLevelUnloaded);
        }

        private void FadeTint()
        {
            float percentage = fadeTimer.time / fadeTimer.interval;
            float alpha = Mathf.Lerp(fadeStartAlpha, tintAlphaTransparent, percentage);

            SetOverlayAlpha(alpha);
        }

        private void SetOverlayAlpha(float a)
        {
            Color overlayColor = overlayImage.color;
            overlayColor.a = a;
            overlayImage.color = overlayColor;
        }

        private void OnHpDecreased(EventData data)
        {
            HealthEventData healthData = data.obj as HealthEventData;

            int hpDecrement = healthData.hpAbsDelta;
            float halfMaxHealth = healthData.maxHp / 2;

            float tintAlphaHeadroom = maxTintAlpha - minTintAlpha;
            fadeStartAlpha = minTintAlpha + (hpDecrement / halfMaxHealth) * tintAlphaHeadroom;
            // clamp in case hpDecrement greater than halfMaxHealth
            fadeStartAlpha = Mathf.Clamp(fadeStartAlpha, minTintAlpha, maxTintAlpha);

            float tintTimeHeadroom = maxTintTime - minTintTime;
            float fadeDuration = minTintTime + (hpDecrement / halfMaxHealth) * tintTimeHeadroom;
            fadeDuration = Mathf.Clamp(fadeDuration, minTintTime, maxTintTime);

            SetOverlayAlpha(fadeStartAlpha);
            fadeTimer.interval = fadeDuration;

            if (fadeTimer.enabled)
                fadeTimer.Reset();
            else
                fadeTimer.Start();
        }

        private void OnLevelUnloaded(EventData data)
        {
            fadeTimer.Stop();
            SetOverlayAlpha(tintAlphaTransparent);
        }
    }
}
