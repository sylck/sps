﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace UI
{
    public static class ButtonEvents
    {
        public const string Hovered = "ui.button.hovered";
        public const string Clicked = "ui.button.clicked";
    }

    public class ButtonEventBroadcaster : MonoBehaviour, IPointerEnterHandler, IPointerClickHandler
    {
        public void OnPointerEnter(PointerEventData eventData)
        {
            EventManager.Invoke(ButtonEvents.Hovered);
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            EventManager.Invoke(ButtonEvents.Clicked);
        }
    }
}
