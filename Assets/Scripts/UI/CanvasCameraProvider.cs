﻿using UnityEngine;

namespace UI
{
    public class CanvasCameraProvider : MonoBehaviour
    {
        [SerializeField]
        private Canvas canvas;

        public Camera GetCamera()
        {
            Camera camera = null;

            if (canvas.renderMode != RenderMode.ScreenSpaceOverlay)
                camera = canvas.worldCamera;

            return camera;
        }
    }
}
