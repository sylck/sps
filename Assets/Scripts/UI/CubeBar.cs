﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class CubeBar : MonoBehaviour
    {
        [SerializeField]
        private GameObject container;

        [SerializeField]
        private Text cubesCollectedLabel, 
                     cubesNeededLabel;

        private float rightmostXPosition,
                      labelXOffset;
        private bool cubesNeededCountInitialized,
                     cubesCollectedCountInitialized;

        private void Awake()
        {
            float cnlXPosition = cubesNeededLabel.rectTransform.anchoredPosition.x,
                  cnlWidth = cubesNeededLabel.preferredWidth;
            rightmostXPosition = cnlXPosition + cnlWidth / 2;

            float cclXPosition = cubesCollectedLabel.rectTransform.anchoredPosition.x,
                  cclWidth = cubesCollectedLabel.preferredWidth;
            labelXOffset = cnlXPosition - cclXPosition - (cnlWidth + cclWidth) / 2;
        }

        private void OnEnable()
        {
            EventManager.AddListener(
                CollectCubesObjectiveEvents.CubesNeededCountInitialized,
                OnCubesNeededCountInitialized);
            EventManager.AddListener(
                CollectCubesObjectiveEvents.CubesNeededCountChanged,
                OnCubesNeededCountChanged);
            EventManager.AddListener(
                CollectCubesObjectiveEvents.CubesCollectedCountInitialized,
                OnCubesCollectedCountInitialized);
            EventManager.AddListener(
                CollectCubesObjectiveEvents.CubesCollectedCountChanged,
                OnCubesCollectedCountChanged);
            EventManager.AddListener(
                EventUtility.TypedEvent(
                    LoadManagerEvents.SceneUnloaded, SceneUtility.SceneType.Level),
                OnLevelUnloaded);
        }

        private void OnDisable()
        {
            EventManager.RemoveListener(
                CollectCubesObjectiveEvents.CubesNeededCountInitialized,
                OnCubesNeededCountInitialized);
            EventManager.RemoveListener(
                CollectCubesObjectiveEvents.CubesNeededCountChanged,
                OnCubesNeededCountChanged);
            EventManager.RemoveListener(
                CollectCubesObjectiveEvents.CubesCollectedCountInitialized,
                OnCubesCollectedCountInitialized);
            EventManager.RemoveListener(
                CollectCubesObjectiveEvents.CubesCollectedCountChanged,
                OnCubesCollectedCountChanged);
            EventManager.RemoveListener(
                EventUtility.TypedEvent(
                    LoadManagerEvents.SceneUnloaded, SceneUtility.SceneType.Level),
                OnLevelUnloaded);
        }

        private void OnCubesNeededCountInitialized(EventData data)
        {
            UpdateCubesNeededLabel(data.intValue);

            cubesNeededCountInitialized = true;
            TryShow();
        }

        private void OnCubesNeededCountChanged(EventData data)
        {
            UpdateCubesNeededLabel(data.intValue);
        }

        private void UpdateCubesNeededLabel(int cubesNeededCount)
        {
            cubesNeededLabel.text = "/ " + cubesNeededCount;

            UpdateCubesNeededLabelPosition();
            UpdateCubesCollectedLabelPosition();
        }

        private void UpdateCubesNeededLabelPosition()
        {
            Vector2 cnlPosition = cubesNeededLabel.rectTransform.anchoredPosition;
            float cnlWidth = cubesNeededLabel.preferredWidth;
            cnlPosition.x = rightmostXPosition - cnlWidth / 2;
            cubesNeededLabel.rectTransform.anchoredPosition = cnlPosition;
        }

        private void OnCubesCollectedCountInitialized(EventData data)
        {
            UpdateCubesCollectedLabel(data.intValue);

            cubesCollectedCountInitialized = true;
            TryShow();
        }

        private void TryShow()
        {
            if (cubesNeededCountInitialized && cubesCollectedCountInitialized)
                GameObjectUtility.Show(container);
        }

        private void OnCubesCollectedCountChanged(EventData data)
        {
            UpdateCubesCollectedLabel(data.intValue);
        }

        private void UpdateCubesCollectedLabel(int cubesCollectedCount)
        {
            cubesCollectedLabel.text = cubesCollectedCount.ToString();

            UpdateCubesCollectedLabelPosition();
        }

        private void UpdateCubesCollectedLabelPosition()
        {
            Vector2 cclPosition = cubesNeededLabel.rectTransform.anchoredPosition;
            float cnlWidth = cubesNeededLabel.preferredWidth;
            float cclWidth = cubesCollectedLabel.preferredWidth;
            cclPosition.x -= labelXOffset + (cnlWidth + cclWidth) / 2;
            cubesCollectedLabel.rectTransform.anchoredPosition = cclPosition;
        }

        private void OnLevelUnloaded(EventData data)
        {
            cubesNeededCountInitialized = false;
            cubesCollectedCountInitialized = false;

            GameObjectUtility.Hide(container);
        }
    }
}
