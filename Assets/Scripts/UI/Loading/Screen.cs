﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UI.Loading
{
    public static class ScreenEvents
    {
        public const string Showing = "ui.loading.screen.showing";
        public const string Hidden = "ui.loading.screen.hidden";
    }

    public class Screen : MonoBehaviour, IDialog
    {
        [SerializeField]
        private GameObject container;

        [SerializeField]
        private DialogHandle dialogHandle;

        private void OnEnable()
        {
            EventManager.AddListener(
                EventUtility.TypedEvent(
                    LoadManagerEvents.SceneLoading, SceneUtility.SceneType.Level),
                OnLevelLoading);
            EventManager.AddListener(
                EventUtility.TypedEvent(
                    LoadManagerEvents.SceneLoaded, SceneUtility.SceneType.Level),
                OnLevelLoaded);
        }

        private void OnDisable()
        {
            EventManager.RemoveListener(
                EventUtility.TypedEvent(
                    LoadManagerEvents.SceneLoading, SceneUtility.SceneType.Level),
                OnLevelLoading);
            EventManager.RemoveListener(
                EventUtility.TypedEvent(
                    LoadManagerEvents.SceneLoaded, SceneUtility.SceneType.Level),
                OnLevelLoaded);
        }

        private void OnLevelLoading(EventData data)
        {
            dialogHandle.ForceOpenPersistent();
        }

        private void OnLevelLoaded(EventData data)
        {
            dialogHandle.Close();
        }

        public void Show()
        {
            EventManager.Invoke(ScreenEvents.Showing);
            GameObjectUtility.Show(container);
        }

        public void Hide()
        {
            GameObjectUtility.Hide(container);
            EventManager.Invoke(ScreenEvents.Hidden);
        }
    }
}
