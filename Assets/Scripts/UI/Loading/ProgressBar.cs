﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Loading
{
    public class ProgressBar : MonoBehaviour
    {
        [SerializeField]
        private Image progressBar;

        // level load process is divided in two stages:
        // 1: unload old level
        // 2: load new
        private const float k_NumStages = 2f;
        private const float k_PerStageProgress = 1f / k_NumStages;

        private const float k_LoadingStarted = 0f * k_PerStageProgress,
                            k_OldLevelUnloading = k_LoadingStarted,
                            k_OldLevelUnloaded = 1f * k_PerStageProgress,
                            k_NewLevelLoading = k_OldLevelUnloaded,
                            k_NewLevelLoaded = 2f * k_PerStageProgress;

        private void OnEnable()
        {
            EventManager.AddListener(
                EventUtility.TypedEvent(
                    LoadManagerEvents.SceneLoading, SceneUtility.SceneType.Level), 
                OnLevelLoading);
            EventManager.AddListener(
                EventUtility.TypedEvent(
                    LoadManagerEvents.SceneUnloadProgress, SceneUtility.SceneType.Level),
                OnLevelUnloadProgress);
            EventManager.AddListener(
                EventUtility.TypedEvent(
                    LoadManagerEvents.SceneUnloaded, SceneUtility.SceneType.Level),
                OnLevelUnloaded);
            EventManager.AddListener(
                EventUtility.TypedEvent(
                    LoadManagerEvents.SceneLoadProgress, SceneUtility.SceneType.Level),
                OnLevelLoadProgress);
            EventManager.AddListener(
                EventUtility.TypedEvent(
                    LoadManagerEvents.SceneLoaded, SceneUtility.SceneType.Level),
                OnLevelLoaded);
        }

        private void OnDisable()
        {
            EventManager.RemoveListener(
                EventUtility.TypedEvent(
                    LoadManagerEvents.SceneLoading, SceneUtility.SceneType.Level),
                OnLevelLoading);
            EventManager.RemoveListener(
                EventUtility.TypedEvent(
                    LoadManagerEvents.SceneUnloadProgress, SceneUtility.SceneType.Level),
                OnLevelUnloadProgress);
            EventManager.RemoveListener(
                EventUtility.TypedEvent(
                    LoadManagerEvents.SceneUnloaded, SceneUtility.SceneType.Level),
                OnLevelUnloaded);
            EventManager.RemoveListener(
                EventUtility.TypedEvent(
                    LoadManagerEvents.SceneLoadProgress, SceneUtility.SceneType.Level),
                OnLevelLoadProgress);
            EventManager.RemoveListener(
                EventUtility.TypedEvent(
                    LoadManagerEvents.SceneLoaded, SceneUtility.SceneType.Level),
                OnLevelLoaded);
        }

        private void OnLevelLoading(EventData data)
        {
            progressBar.fillAmount = k_LoadingStarted;
        }

        private void OnLevelUnloadProgress(EventData data)
        {
            float unloadProgress = data.floatValue;
            progressBar.fillAmount = k_OldLevelUnloading + unloadProgress * k_PerStageProgress;
        }

        private void OnLevelUnloaded(EventData data)
        {
            progressBar.fillAmount = k_OldLevelUnloaded;
        }

        private void OnLevelLoadProgress(EventData data)
        {
            float loadProgress = data.floatValue;
            progressBar.fillAmount = k_NewLevelLoading + loadProgress * k_PerStageProgress;
        }

        private void OnLevelLoaded(EventData data)
        {
            progressBar.fillAmount = k_NewLevelLoaded;
        }
    }
}
