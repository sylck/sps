﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UI
{
    public class HideOnMenuShowing : MonoBehaviour
    {
        [SerializeField]
        private GameObject objectToHide;

        private void OnEnable()
        {
            EventManager.AddListener(MenuEvents.Showing, OnMenuShowing);
            EventManager.AddListener(MenuEvents.Hidden, OnMenuHidden);
        }

        private void OnDisable()
        {
            EventManager.RemoveListener(MenuEvents.Showing, OnMenuShowing);
            EventManager.RemoveListener(MenuEvents.Hidden, OnMenuHidden);
        }

        private void OnMenuShowing(EventData data)
        {
            GameObjectUtility.Hide(objectToHide);
        }

        private void OnMenuHidden(EventData data)
        {
            GameObjectUtility.Show(objectToHide);
        }
    }
}
