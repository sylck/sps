﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UI
{
    public class MenuGameEventHandler : MonoBehaviour
    {
        [SerializeField]
        private Menu menu;

        [SerializeField]
        private DialogHandle menuHandle;

        private void OnEnable()
        {
            EventManager.AddListener(
                GameManagerEvents.EnteredDeathState, OnEnteredDeathState);
            EventManager.AddListener(
                GameManagerEvents.EnteredVictoryState, OnEnteredVictoryState);
        }

        private void OnDisable()
        {
            EventManager.RemoveListener(
                GameManagerEvents.EnteredDeathState, OnEnteredDeathState);
            EventManager.RemoveListener(
                GameManagerEvents.EnteredVictoryState, OnEnteredVictoryState);
        }

        private void OnEnteredDeathState(EventData data)
        {
            menu.SetLayout(Menu.Layout.DeathState);
            menuHandle.ForceOpenPersistent();
        }

        private void OnEnteredVictoryState(EventData data)
        {
            menu.SetLayout(Menu.Layout.VictoryState);
            menuHandle.ForceOpenPersistent();
        }
    }
}
