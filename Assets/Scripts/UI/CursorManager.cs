﻿using UnityEngine;

namespace UI
{
    public class CursorManager : MonoBehaviour
    {
        private void OnEnable()
        {
            EventManager.AddListener(
                MenuEvents.Showing, 
                OnDialogShowing);
            EventManager.AddListener(
                Inventory.UI.InventoryDialogEvents.Showing, 
                OnDialogShowing);

            EventManager.AddListener(
                Inventory.UI.DragIndicatorEvents.Showing, 
                HideCursorHandler);
            EventManager.AddListener(
                Inventory.UI.DragIndicatorEvents.Hidden,
                ShowCursorHandler);

            EventManager.AddListener(
                DialogEvents.Closed, 
                OnDialogClosed);
        }

        private void OnDisable()
        {
            EventManager.RemoveListener(
                MenuEvents.Showing,
                OnDialogShowing);
            EventManager.RemoveListener(
                Inventory.UI.InventoryDialogEvents.Showing,
                OnDialogShowing);

            EventManager.RemoveListener(
                Inventory.UI.DragIndicatorEvents.Showing,
                HideCursorHandler);
            EventManager.RemoveListener(
                Inventory.UI.DragIndicatorEvents.Hidden,
                ShowCursorHandler);

            EventManager.RemoveListener(
                DialogEvents.Closed,
                OnDialogClosed);
        }

        private void SetCursorVisibility(bool visible)
        {
            Cursor.visible = visible;
        }

        private void ShowCursor()
        {
            SetCursorVisibility(true);
        }

        private void HideCursor()
        {
            SetCursorVisibility(false);
        }

        private void SetCursorLockState(CursorLockMode cursorLockState)
        {
            Cursor.lockState = cursorLockState;
        }

        private void UnlockCursor()
        {
            SetCursorLockState(CursorLockMode.None);
        }

        private void LockCursor()
        {
            SetCursorLockState(CursorLockMode.Locked);
        }

        private void OnDialogShowing(EventData data)
        {
            UnlockCursor();
            ShowCursor();
        }

        private void HideCursorHandler(EventData data)
        {
            HideCursor();
        }

        private void ShowCursorHandler(EventData data)
        {
            ShowCursor();
        }

        private void OnDialogClosed(EventData data)
        {
            LockCursor();
            HideCursor();
        }
    }
}
