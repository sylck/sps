﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class HealthBar : MonoBehaviour
    {
        [SerializeField]
        private GameObject container;

        [SerializeField]
        private Text hpLabel, 
                     maxHpLabel;

        [SerializeField]
        private RectTransform bar, 
                              deltaBar;

        [SerializeField]
        private Image barImage,
                      deltaBarImage;

        [SerializeField]
        private Color deltaBarLossColor,
                      deltaBarGainColor;

        [SerializeField]
        private float initialDeltaBarAlpha = .64f,
                      deltaBarFadeDuration = .5f;

        private float leftmostBarAreaXPosition, barRectWidth, deltaBarRectWidth,
                      rightmostLabelAreaXPosition, labelXOffset;
        private Timer deltaBarFadeTimer;

        private const float fullBarFillAmount = 1f;
        private const float deltaBarAlphaTransparent = 0f;

        private void Awake()
        {
            deltaBarFadeTimer = new Timer.Builder()
                .SetInterval(deltaBarFadeDuration)
                .SetAutoReset(false)
                .AddTickedListener(FadeDeltaBar)
                .Build();

            float barXPosition = bar.anchoredPosition.x;
            barRectWidth = bar.rect.width;
            deltaBarRectWidth = deltaBar.rect.width;
            leftmostBarAreaXPosition = barXPosition - barRectWidth / 2;

            float mhlXPosition = maxHpLabel.rectTransform.anchoredPosition.x,
                  mhlWidth = maxHpLabel.preferredWidth;
            rightmostLabelAreaXPosition = mhlXPosition + mhlWidth / 2;

            float hlXPosition = hpLabel.rectTransform.anchoredPosition.x;
            float hlWidth = hpLabel.preferredWidth;
            labelXOffset = mhlXPosition - hlXPosition - (mhlWidth + hlWidth) / 2;
        }

        private void OnEnable()
        {
            EventManager.AddListener(
                EventUtility.TypedEvent(
                    HealthEvents.Initialized, ThingType.Player),
                OnHealthInitialized);
            EventManager.AddListener(
                EventUtility.TypedEvent(
                    HealthEvents.HpChanged, ThingType.Player), 
                OnHpChanged);
            EventManager.AddListener(
                EventUtility.TypedEvent(
                    LoadManagerEvents.SceneUnloaded, SceneUtility.SceneType.Level),
                OnLevelUnloaded);
        }

        // Update is called once per frame
        void Update() 
        {
            deltaBarFadeTimer.Tick(Time.deltaTime);
        }

        private void OnDisable()
        {
            EventManager.RemoveListener(
                EventUtility.TypedEvent(
                    HealthEvents.Initialized, ThingType.Player),
                OnHealthInitialized);
            EventManager.RemoveListener(
                EventUtility.TypedEvent(
                    HealthEvents.HpChanged, ThingType.Player), 
                OnHpChanged);
            EventManager.RemoveListener(
                EventUtility.TypedEvent(
                    LoadManagerEvents.SceneUnloaded, SceneUtility.SceneType.Level),
                OnLevelUnloaded);
        }

        private void FadeDeltaBar()
        {
            float percentage = deltaBarFadeTimer.time / deltaBarFadeTimer.interval;
            float alpha = Mathf.Lerp(initialDeltaBarAlpha, deltaBarAlphaTransparent, percentage);
            SetDeltaBarAlpha(alpha);
        }

        private void SetDeltaBarAlpha(float a)
        {
            Color lossBarColor = deltaBarImage.color;
            lossBarColor.a = a;
            deltaBarImage.color = lossBarColor;
        }

        private void SetDeltaBarColor(Color color)
        {
            deltaBarImage.color = color;
        }

        private void UpdateHealthBar(int hp, int maxHp)
        {
            barImage.fillAmount = (float)hp / maxHp;
        }

        private void UpdateMaxHpLabel(int maxHp)
        {
            maxHpLabel.text = "/ " + maxHp;

            Vector3 mhlPosition = maxHpLabel.rectTransform.anchoredPosition;
            float mhlWidth = maxHpLabel.preferredWidth;
            mhlPosition.x = rightmostLabelAreaXPosition - mhlWidth / 2;
            maxHpLabel.rectTransform.anchoredPosition = mhlPosition;
        }

        private void UpdateHpLabel(int hp)
        {
            hpLabel.text = hp.ToString();

            Vector2 hlPosition = maxHpLabel.rectTransform.anchoredPosition;
            float mhlWidth = maxHpLabel.preferredWidth;
            float hlWidth = hpLabel.preferredWidth;
            hlPosition.x -= labelXOffset + (mhlWidth + hlWidth) / 2;
            hpLabel.rectTransform.anchoredPosition = hlPosition;
        }

        private void UpdateFadeDeltaBar(int hp, int hpDelta, int hpAbsDelta, int maxHp)
        {
            bool isGain = hpDelta > 0;

            float deltaBarScaleX = (float)hpAbsDelta / maxHp;
            deltaBarImage.fillAmount = deltaBarScaleX;

            Vector2 deltaBarPosition = deltaBar.anchoredPosition;
            float barScaleX = (float)hp / maxHp;
            float apparentBarRectWidth = barRectWidth * barScaleX;
            float apparentDeltaBarRectWidth = deltaBarRectWidth * deltaBarScaleX;

            deltaBarPosition.x = leftmostBarAreaXPosition + apparentBarRectWidth + 
                deltaBarRectWidth / 2;
        
            if (isGain)
                deltaBarPosition.x -= apparentDeltaBarRectWidth;

            deltaBar.anchoredPosition = deltaBarPosition;

            if (isGain)
                SetDeltaBarColor(deltaBarGainColor);
            else
                SetDeltaBarColor(deltaBarLossColor);

            SetDeltaBarAlpha(initialDeltaBarAlpha);

            if (deltaBarFadeTimer.enabled)
                deltaBarFadeTimer.Reset();
            else
                deltaBarFadeTimer.Start();
        }

        private void OnHealthInitialized(EventData data)
        {
            HealthEventData healthData = data.obj as HealthEventData;
        
            UpdateMaxHpLabel(healthData.maxHp);
            UpdateHpLabel(healthData.hp);

            GameObjectUtility.Show(container);
        }

        private void OnHpChanged(EventData data)
        {
            HealthEventData healthData = data.obj as HealthEventData;

            UpdateHealthBar(healthData.hp, healthData.maxHp);
            UpdateHpLabel(healthData.hp);
            UpdateFadeDeltaBar(healthData.hp, healthData.hpDelta, healthData.hpAbsDelta, 
                healthData.maxHp);
        }

        private void OnLevelUnloaded(EventData data)
        {
            deltaBarFadeTimer.Stop();
            barImage.fillAmount = fullBarFillAmount;
            deltaBarImage.fillAmount = fullBarFillAmount;
            SetDeltaBarAlpha(deltaBarAlphaTransparent);

            GameObjectUtility.Hide(container);
        }
    }
}
