﻿namespace UI
{
    public interface IDialog
    {
        // Show(), Hide() - methods intended to be called strictly from within DialogHandle. 
        // should perform preparation work and make relevant associated GameObjects visible or 
        // hide them and, when necessary, do some cleanup work. code within these methods should 
        // not care about other dialogs' visibility as keeping track of open dialogs is duty of 
        // DialogHandle and DialogManager classes. 
        void Show();
        void Hide();
    }
}
