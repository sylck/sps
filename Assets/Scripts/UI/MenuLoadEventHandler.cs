﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace UI
{
    public class MenuLoadEventHandler : MonoBehaviour
    {
        [SerializeField]
        private Menu menu;

        [SerializeField]
        private DialogHandle menuHandle;

        private void OnEnable()
        {
            EventManager.AddListener(
                EventUtility.TypedEvent(
                    LoadManagerEvents.SceneLoaded, SceneUtility.SceneType.MainMenu), 
                OnMainMenuLoaded);
            EventManager.AddListener(
                EventUtility.TypedEvent(
                    LoadManagerEvents.SceneLoading, SceneUtility.SceneType.Level),
                OnLevelLoading);
        }

        private void OnDisable()
        {
            EventManager.RemoveListener(
                EventUtility.TypedEvent(
                    LoadManagerEvents.SceneLoaded, SceneUtility.SceneType.MainMenu), 
                OnMainMenuLoaded);
            EventManager.RemoveListener(
                EventUtility.TypedEvent(
                    LoadManagerEvents.SceneLoading, SceneUtility.SceneType.Level),
                OnLevelLoading);
        }

        private void OnMainMenuLoaded(EventData data)
        {
            menu.SetLayout(Menu.Layout.MainMenu);
            menuHandle.ForceOpenPersistent();
        }

        private void OnLevelLoading(EventData data)
        {
            menu.SetLayout(Menu.Layout.PauseMenu);
            menuHandle.TryClose();
            menuHandle.SetPersistent(false);
        }
    }
}
