﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UI
{
    public class DialogManager : MonoBehaviour
    {
        private DialogHandle m_ActiveDialogHandle;
        public DialogHandle activeDialogHandle { get { return m_ActiveDialogHandle;  } }

        private List<DialogHandle> dialogHandles;

        private void Awake()
        {
            dialogHandles = new List<DialogHandle>();
        }

        private void Update()
        {
            // if a persistent (non-dismissable) dialog is open, do nothing
            bool persistentDialogOpen = HasActiveDialog() && 
                activeDialogHandle.flags == DialogFlags.Persistent;
            if (persistentDialogOpen)
                return;

            // if something is open and player dismisses it, that's it for this iteration
            bool dismissed = TryDismissActiveDialog();
            if (dismissed)
                return;

            // assuming there is an active dialog, is player pressing its close key?
            if (HasActiveDialog())
            {
                if (Input.GetKeyDown(activeDialogHandle.toggleVisibilityKey))
                    CloseActiveDialog();

                return;
            }

            // no active dialog, scan if player pressed any of registered open keys
            for (int i = 0; i < dialogHandles.Count; i++)
            {
                DialogHandle dialogHandle = dialogHandles[i];

                if (!Input.GetKeyDown(dialogHandle.toggleVisibilityKey))
                    continue;

                OpenDialog(dialogHandle);
                break;
            }
        }

        public bool HasActiveDialog()
        {
            return activeDialogHandle != null;
        }

        private bool TryDismissActiveDialog()
        {
            if (!GetDismissInput())
                return false;

            if (!HasActiveDialog())
                return false;

            CloseActiveDialog();
            return true;
        }

        public void TryCloseActiveDialog()
        {
            if (!HasActiveDialog())
                return;

            CloseActiveDialog();
        }

        private void CloseActiveDialog()
        {
            activeDialogHandle.Close();
        }

        public void UnsetActiveDialog()
        {
            m_ActiveDialogHandle = null;
        }

        private bool GetDismissInput()
        {
            return Input.GetKeyDown(KeyCode.Escape);
        }

        private void OpenDialog(DialogHandle dialogHandle)
        {
            dialogHandle.Open();
        }

        public void SetActiveDialogHandle(DialogHandle handle)
        {
            m_ActiveDialogHandle = handle;
        }

        public void Register(DialogHandle handle)
        {
            dialogHandles.Add(handle);
        }
    }
}
