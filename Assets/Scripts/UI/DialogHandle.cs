﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UI
{
    public static class DialogEvents
    {
        public const string Opening = "ui.dialog.opening";
        public const string Closed = "ui.dialog.closed";
    }

    public class DialogHandle : MonoBehaviour
    {
        [SerializeField]
        private MonoBehaviour dialogComponent;

        [SerializeField]
        private DialogManager dialogManager;
        
        [SerializeField]
        private KeyCode m_ToggleVisibilityKey;
        public KeyCode toggleVisibilityKey { get { return m_ToggleVisibilityKey; } }

        private DialogFlags m_Flags = DialogFlags.Dismissable;
        public DialogFlags flags { get { return m_Flags; } }

        private IDialog dialog;

        public void Awake()
        {
            dialog = (IDialog)dialogComponent;
        }

        public void Start()
        {
            dialogManager.Register(this);
        }

        public void TryOpen()
        {
            if (dialogManager.HasActiveDialog())
                return;

            Open();
        }

        public void Open()
        {
            EventManager.Invoke(DialogEvents.Opening);
            dialog.Show();
            dialogManager.SetActiveDialogHandle(this);
        }

        public void TryClose()
        {
            if (!dialogManager.activeDialogHandle == this)
                return;

            Close();
        }

        public void Close()
        {
            dialog.Hide();
            dialogManager.UnsetActiveDialog();
            EventManager.Invoke(DialogEvents.Closed);
        }

        public void ForceOpen()
        {
            dialogManager.TryCloseActiveDialog();
            Open();
        }

        public void ForceOpenPersistent()
        {
            ForceOpen();
            SetPersistent(true);
        }

        public void SetPersistent(bool persistent)
        {
            if (persistent)
                m_Flags = DialogFlags.Persistent;
            else
                m_Flags = DialogFlags.Dismissable;
        }
    }
}
