﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class AmmoBar : MonoBehaviour
    {
        [SerializeField]
        private GameObject container;

        [SerializeField]
        private Text ammoLabel;

        private float rightmostXPosition;

        private void Awake()
        {
            float ammoLabelXPosition = ammoLabel.rectTransform.anchoredPosition.x,
                  ammoLabelWidth = ammoLabel.preferredWidth;
            rightmostXPosition = ammoLabelXPosition + ammoLabelWidth / 2;
        }

        private void OnEnable()
        {
            EventManager.AddListener(
                EventUtility.TypedEvent(
                    PlayerInventoryEvents.ItemCountInitialized, ThingType.SphereOrange),
                OnAmmoCountInitialized);
            EventManager.AddListener(
                EventUtility.TypedEvent(
                    PlayerInventoryEvents.ItemCountChanged, ThingType.SphereOrange),
                OnAmmoCountChanged);
            EventManager.AddListener(
                EventUtility.TypedEvent(
                    LoadManagerEvents.SceneUnloaded, SceneUtility.SceneType.Level),
                OnLevelUnloaded);
        }

        private void OnDisable()
        {
            EventManager.RemoveListener(
                EventUtility.TypedEvent(
                    PlayerInventoryEvents.ItemCountInitialized, ThingType.SphereOrange),
                OnAmmoCountInitialized);
            EventManager.RemoveListener(
                EventUtility.TypedEvent(
                    PlayerInventoryEvents.ItemCountChanged, ThingType.SphereOrange),
                OnAmmoCountChanged);
            EventManager.RemoveListener(
                EventUtility.TypedEvent(
                    LoadManagerEvents.SceneUnloaded, SceneUtility.SceneType.Level),
                OnLevelUnloaded);
        }

        private void OnAmmoCountInitialized(EventData data)
        {
            UpdateAmmoLabel(data.intValue);

            GameObjectUtility.Show(container);
        }

        private void OnAmmoCountChanged(EventData data)
        {
            UpdateAmmoLabel(data.intValue);
        }

        private void UpdateAmmoLabel(int ammoCount)
        {
            ammoLabel.text = ammoCount.ToString();

            UpdateAmmoLabelPosition();
        }

        private void UpdateAmmoLabelPosition()
        {
            Vector2 ammoLabelPosition = ammoLabel.rectTransform.anchoredPosition;
            float ammoLabelWidth = ammoLabel.preferredWidth;
            ammoLabelPosition.x = rightmostXPosition - ammoLabelWidth / 2;
            ammoLabel.rectTransform.anchoredPosition = ammoLabelPosition;
        }

        private void OnLevelUnloaded(EventData data)
        {
            GameObjectUtility.Hide(container);
        }
    }
}
