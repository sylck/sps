﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ThingType
{
    Undefined,

    Player,
    CylinderWhite,

    CubeGreen,
    CubeYellow,
    SphereOrange,
    SpherePurple
};

public static class ThingEvents
{
    public const string Started = "thing.started";
}

public class Thing : MonoBehaviour
{
    [SerializeField]
    private ThingType m_Type;
    public ThingType type { get { return m_Type; } }

    private void Start()
    {
        EventManager.Invoke(
            EventUtility.TypedEvent(ThingEvents.Started, m_Type));
    }
}
