﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(GameManager))]
public class GameManagerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        GameManager gameManager = (GameManager)target;

        EditorGUILayout.Space();
        EditorGUILayout.LabelField("Set State", EditorStyles.boldLabel);
        using (new EditorGUI.DisabledScope(!Application.isPlaying))
        {
            EditorGUILayout.BeginHorizontal();
            GUILayout.Label("Player", GUILayout.Width(EditorGUIUtility.labelWidth));
            if (GUILayout.Button("Dies", EditorStyles.miniButtonLeft))
                gameManager.TrySetDeathState();
            if (GUILayout.Button("Victorious", EditorStyles.miniButtonRight))
                gameManager.TrySetVictoryState();
            EditorGUILayout.EndHorizontal();
        }
    }
}
