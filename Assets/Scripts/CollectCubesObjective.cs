﻿public static class CollectCubesObjectiveEvents
{
    public const string CubesNeededCountInitialized = "collectCubesObjective.cubesNeededCountInitialized";
    public const string CubesNeededCountChanged = "collectCubesObjective.cubesNeededCountChanged";
    public const string CubesCollectedCountInitialized = "collectCubesObjective.cubesCollectedCountInitialized";
    public const string CubesCollectedCountChanged = "collectCubesObjective.cubesCollectedCountChanged";
}

public class CollectCubesObjective : Objective
{
    private int cubeGreenCollectedCount, cubeGreenTotalCount,
                cubeYellowCollectedCount, cubeYellowTotalCount,
                cubesCollectedCount, cubesNeededCount;

    private void OnEnable()
    {
        EventManager.AddListener(
            EventUtility.TypedEvent(ThingEvents.Started, ThingType.CubeGreen),
            OnCubeGreenStarted);
        EventManager.AddListener(
            EventUtility.TypedEvent(ThingEvents.Started, ThingType.CubeYellow),
            OnCubeYellowStarted);
        EventManager.AddListener(
            EventUtility.TypedEvent(PlayerInventoryEvents.ItemCountChanged, ThingType.CubeGreen),
            OnCubeGreenCollectedCountChanged);
        EventManager.AddListener(
            EventUtility.TypedEvent(PlayerInventoryEvents.ItemCountChanged, ThingType.CubeYellow),
            OnCubeYellowCollectedCountChanged);
    }

    private void Start()
    {
        EventManager.Invoke(
            CollectCubesObjectiveEvents.CubesNeededCountInitialized,  
            cubesNeededCount);
        EventManager.Invoke(
            CollectCubesObjectiveEvents.CubesCollectedCountInitialized, 
            cubesCollectedCount);
    }

    private void OnDisable()
    {
        EventManager.RemoveListener(
            EventUtility.TypedEvent(ThingEvents.Started, ThingType.CubeGreen),
            OnCubeGreenStarted);
        EventManager.RemoveListener(
            EventUtility.TypedEvent(ThingEvents.Started, ThingType.CubeYellow),
            OnCubeYellowStarted);
        EventManager.RemoveListener(
            EventUtility.TypedEvent(PlayerInventoryEvents.ItemCountChanged, ThingType.CubeGreen),
            OnCubeGreenCollectedCountChanged);
        EventManager.RemoveListener(
            EventUtility.TypedEvent(PlayerInventoryEvents.ItemCountChanged, ThingType.CubeYellow),
            OnCubeYellowCollectedCountChanged);
    }

    private void OnCubeGreenStarted(EventData data)
    {
        cubeGreenTotalCount++;
        CubesNeededCountChanged();
    }

    private void OnCubeYellowStarted(EventData data)
    {
        cubeYellowTotalCount++;
        CubesNeededCountChanged();
    }

    private void CubesNeededCountChanged()
    {
        cubesNeededCount = cubeGreenTotalCount + cubeYellowTotalCount;

        EventManager.Invoke(
            CollectCubesObjectiveEvents.CubesNeededCountChanged, 
            cubesNeededCount);
    }

    private void OnCubeGreenCollectedCountChanged(EventData data)
    {
        cubeGreenCollectedCount = data.intValue;
        CubesCollectedCountChanged();
    }

    private void OnCubeYellowCollectedCountChanged(EventData data)
    {
        cubeYellowCollectedCount = data.intValue;
        CubesCollectedCountChanged();
    }

    private void CubesCollectedCountChanged()
    {
        cubesCollectedCount = cubeGreenCollectedCount + cubeYellowCollectedCount;

        EventManager.Invoke(
            CollectCubesObjectiveEvents.CubesCollectedCountChanged,
            cubesCollectedCount);

        TryRaiseCompleted();
    }

    public override bool IsCompleted()
    {
        return cubesCollectedCount == cubesNeededCount;
    }
}
