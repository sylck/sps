﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EventData
{
    private int m_IntValue;
    public int intValue { get { return m_IntValue; } }

    private float m_FloatValue;
    public float floatValue { get { return m_FloatValue; } }

    private object m_Obj;
    public object obj { get { return m_Obj; } }

    public EventData(int intValue)
    {
        m_IntValue = intValue;
    }

    public EventData(float floatValue)
    {
        m_FloatValue = floatValue;
    }

    public EventData(object obj)
    {
        m_Obj = obj;
    }
}

public static class EventUtility
{
    public static string TypedEvent(string eventName, ThingType type)
    {
        return TypedEvent(eventName, type.ToString());
    }

    public static string TypedEvent(string eventName, SceneUtility.SceneType type)
    {
        return TypedEvent(eventName, type.ToString());
    }

    private static string TypedEvent(string eventName, string typeString)
    {
        string prefix = typeString;
        prefix = char.ToLower(typeString[0]) + typeString.Substring(1);
        return prefix + "." + eventName;
    }
}

public class Event : UnityEvent<EventData> {}

public class EventManager : MonoBehaviour
{
    private Dictionary<string, Event> events;
    private static bool s_InitializeSuccessful = false,
                        s_InitializeAttempted = false;
    private static EventManager s_Instance;

    private static bool Initialize()
    {
        s_Instance = FindObjectOfType<EventManager>();

        if (s_Instance == null)
        {
            Debug.Log(
                "EventManager has not been attached to a GameObject. Further method calls will " +
                "be ignored."
            );
            return false;
        }

        s_Instance.events = new Dictionary<string, Event>();
        return true;
    }

    private static bool TryInitialize()
    {
        if (!s_InitializeAttempted)
        {
            s_InitializeSuccessful = Initialize();
            s_InitializeAttempted = true;
        }

        return s_InitializeSuccessful;
    }

    public static void AddListener(string eventName, UnityAction<EventData> listener)
    {
        if (!TryInitialize()) 
            return;
        
        Event e;

        if (!s_Instance.events.TryGetValue(eventName, out e))
        {
            e = new Event();
            s_Instance.events.Add(eventName, e);
        }
            
        e.AddListener(listener);
    }

    public static void RemoveListener(string eventName, UnityAction<EventData> listener)
    {
        if (!TryInitialize()) 
            return;

        Event e;

        if (s_Instance.events.TryGetValue(eventName, out e))
            e.RemoveListener(listener);
    }

    public static void Invoke(string eventName, int value)
    {
        if (!TryInitialize())
            return;

        TryInvoke(eventName, new EventData(value));
    }

    public static new void Invoke(string eventName, float value)
    {
        if (!TryInitialize())
            return;

        TryInvoke(eventName, new EventData(value));
    }

    public static void Invoke(string eventName, object obj = null)
    {
        if (!TryInitialize()) 
            return;

        TryInvoke(eventName, new EventData(obj));
    }

    private static void TryInvoke(string eventName, EventData data)
    {
        Event e;

        if (!s_Instance.events.TryGetValue(eventName, out e))
            return;

        e.Invoke(data);
    }
}
