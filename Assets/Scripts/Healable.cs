﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Healable : MonoBehaviour
{
    [SerializeField]
    private Health health;

    public void Heal(int hpIncrement)
    {
        health.AddHp(hpIncrement);
    }
}
