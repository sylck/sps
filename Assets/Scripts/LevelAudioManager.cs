﻿using UnityEngine;
using UnityEngine.Events;

public class LevelAudioManager : MonoBehaviour
{
    [SerializeField]
    private float fadeTime = .25f;

    private Timer fadeTimer;
    private float startVolume, endVolume;
    private UnityAction fadeEndedHandler;

    private void OnEnable()
    {
        EventManager.AddListener(GameManagerEvents.GameWillStopRunning, OnGameWillStopRunning);
        EventManager.AddListener(GameManagerEvents.GameWillStartRunning, OnGameWillStartRunning);
    }

    // Start is called before the first frame update
    void Start()
    {
        fadeTimer = new Timer.Builder()
            .SetInterval(fadeTime)
            .SetAutoReset(false)
            .AddTickedListener(PerformFadeStep)
            .AddElapsedListener(fadeEndedHandler)
            .Build();
    }

    // Update is called once per frame
    void Update()
    {
        fadeTimer.Tick(Time.unscaledDeltaTime);
    }

    private void OnDisable()
    {
        EventManager.RemoveListener(GameManagerEvents.GameWillStopRunning, OnGameWillStopRunning);
        EventManager.RemoveListener(GameManagerEvents.GameWillStartRunning, OnGameWillStartRunning);
    }

    private void PerformFadeStep()
    {
        float percentage = fadeTimer.time / fadeTimer.interval;
        float volume = Mathf.Lerp(startVolume, endVolume, percentage);
        
        AudioListener.volume = volume;
    }

    private void PauseListener()
    {
        SetAudioListenerPause(true);
    }

    private void UnpauseListener()
    {
        SetAudioListenerPause(false);
    }

    private void SetAudioListenerPause(bool pause)
    {
        AudioListener.pause = pause;
    }

    private void Fade(float from, float to, UnityAction endedCallback)
    {
        startVolume = from;
        endVolume = to;
        fadeEndedHandler = endedCallback;

        fadeTimer.Start();
    }

    private void OnGameWillStopRunning(EventData data)
    {
        Pause();
    }

    private void OnGameWillStartRunning(EventData data)
    {
        Unpause();
    }

    private void Pause()
    {
        Fade(GameConstants.Volume.Maximum, GameConstants.Volume.Silent, PauseListener);
    }

    private void Unpause()
    {
        Fade(GameConstants.Volume.Silent, GameConstants.Volume.Maximum, UnpauseListener);
    }
}
