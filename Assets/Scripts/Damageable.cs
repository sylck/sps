﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum DamageType
{
    Projectile,
    Zone
}

public class Damageable : MonoBehaviour
{
    [SerializeField]
    protected Health health;

    [SerializeField]
    private bool susceptibleToProjectileDamage, 
                 susceptibleToZoneDamage;

    public void OnEnable()
    {
        health.zeroHpReached += OnZeroHpReached;
    }

    public void OnDisable()
    {
        health.zeroHpReached -= OnZeroHpReached;
    }

    private void OnZeroHpReached()
    {
        // disable self to prevent taking further damage
        enabled = false;
    }

    public void TakeDamage(DamageType damageType, int hpDecrement)
    {
        if (!SusceptibleTo(damageType))
            return;

        health.SubtractHp(hpDecrement);
    }

    public bool SusceptibleTo(DamageType damageType)
    {
        switch (damageType)
        {
            case DamageType.Projectile:
                return susceptibleToProjectileDamage;
            case DamageType.Zone:
                return susceptibleToZoneDamage;
            default:
                return false;
        }
    }
}
