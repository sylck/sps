﻿public static class GameConstants
{
    public static class TimeScale
    {
        public const float Stopped = 0f;
        public const float Realtime = 1f;
    }

    public static class Volume
    {
        public const float Silent = 0f;
        public const float Maximum = 1f;
    }
}
