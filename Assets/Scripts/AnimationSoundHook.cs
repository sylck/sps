﻿using UnityEngine;

public class AnimationSoundHook : MonoBehaviour
{
    [SerializeField]
    private AudioSource audioSource;
    
    [SerializeField]
    private AudioClip sound;

    private void OnEnable()
    {
        audioSource.PlayOneShot(sound);
    }
}
