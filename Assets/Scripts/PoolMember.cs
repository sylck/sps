﻿using System;
using UnityEngine;

public class PoolMember : MonoBehaviour
{
    [NonSerialized]
    public int originalId;

    public GameObject GetInstance()
    {
        GameObject instance = Pool.GetInstance(this);

        if (instance == null)
            instance = MakeInstance();

        return instance;
    }

    public GameObject MakeInstance()
    {
        PoolMember instance = Instantiate(this);
        instance.originalId = GetInstanceID();
        return instance.gameObject;
    }

    public void ReturnToPool()
    {
        if (!Pool.Reclaim(this))
            Destroy(gameObject);
    }
}
