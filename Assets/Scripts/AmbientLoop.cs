﻿using UnityEngine;

public class AmbientLoop : MonoBehaviour
{
    [SerializeField]
    private AudioClip[] loopClips;

    [SerializeField]
    private AudioSource primarySource,
                        secondarySource;

    [SerializeField]
    private float fadeTime = .5f;

    private bool playing;
    private AudioSource activeSource,
                        nextSource;
    private double activeSourceEndTime, 
                   nextSourceEndTime;
    private Timer activeSourceFadeOutTimer;
    private SceneUtility.SceneType lastSceneType = SceneUtility.SceneType.Undefined;

    private void OnEnable()
    {
        EventManager.AddListener(
            EventUtility.TypedEvent(LoadManagerEvents.SceneLoaded, SceneUtility.SceneType.MainMenu), 
            OnMainMenuLoaded);
        EventManager.AddListener(
            EventUtility.TypedEvent(LoadManagerEvents.SceneLoaded, SceneUtility.SceneType.Level), 
            OnLevelLoaded);
    }

    private void Start()
    {
        activeSourceFadeOutTimer = new Timer.Builder()
            .SetInterval(fadeTime)
            .SetAutoReset(false)
            .AddTickedListener(PerformFadeOutStep)
            .AddElapsedListener(FinalizeLoopStop)
            .Build();

        primarySource.ignoreListenerPause = true;
        primarySource.ignoreListenerVolume = true;
        secondarySource.ignoreListenerPause = true;
        secondarySource.ignoreListenerVolume = true;
    }

    private void Update()
    {
        activeSourceFadeOutTimer.Tick(Time.unscaledDeltaTime);

        if (!playing)
            return;

        PrepareNextSource();
        UpdateActiveSource();
    }

    private void OnDisable()
    {
        EventManager.RemoveListener(
            EventUtility.TypedEvent(LoadManagerEvents.SceneLoaded, SceneUtility.SceneType.MainMenu),
            OnMainMenuLoaded);
        EventManager.RemoveListener(
            EventUtility.TypedEvent(LoadManagerEvents.SceneLoaded, SceneUtility.SceneType.Level),
            OnLevelLoaded);
    }

    private AudioSource GetSource()
    {
        if (activeSource != primarySource)
            return primarySource;
        else
            return secondarySource;
    }

    private AudioClip GetRandomClip()
    {
        int index = Random.Range(0, loopClips.Length);
        return loopClips[index];
    }

    private double GetClipLength(AudioClip clip)
    {
        return (double)clip.samples / clip.frequency;
    }

    private void PrepareNextSource()
    {
        double dspTime = AudioSettings.dspTime;

        // leave if:
        // * current clip too far from end to prepare next clip
        //  or
        // * next source already scheduled
        if (dspTime < activeSourceEndTime - 1d || nextSource != null)
            return;

        nextSource = GetSource();

        AudioClip nextClip = GetRandomClip();
        nextSource.clip = nextClip;

        nextSourceEndTime = activeSourceEndTime + GetClipLength(nextClip);
        nextSource.PlayScheduled(activeSourceEndTime);
    }

    private void SetActiveSource(AudioSource source, double endTime)
    {
        activeSource = source;
        activeSourceEndTime = endTime;
    }

    private void UpdateActiveSource()
    {
        double dspTime = AudioSettings.dspTime;

        // if current source still playing, leave
        if (dspTime < activeSourceEndTime)
            return;

        SetActiveSource(nextSource, nextSourceEndTime);
        nextSource = null;
    }

    private void StartLoop()
    {
        AudioSource source = GetSource();

        source.volume = GameConstants.Volume.Maximum;

        AudioClip clip = GetRandomClip();
        source.clip = clip;

        double startTime = AudioSettings.dspTime,
               endTime = startTime + GetClipLength(clip);
        source.PlayScheduled(startTime);

        SetActiveSource(source, endTime);

        playing = true;
    }

    private void ForceStopLoop()
    {
        activeSourceFadeOutTimer.Stop();
        activeSource.volume = GameConstants.Volume.Silent;
        FinalizeLoopStop();
    }

    private void OnMainMenuLoaded(EventData data)
    {
        bool lastSceneIsLevel = lastSceneType == SceneUtility.SceneType.Level;
        if (lastSceneIsLevel && activeSourceFadeOutTimer.enabled)
            ForceStopLoop();

        bool lastSceneIsNotMainMenu = lastSceneType != SceneUtility.SceneType.MainMenu;
        if (lastSceneIsNotMainMenu)
            StartLoop();

        lastSceneType = SceneUtility.SceneType.MainMenu;
    }

    private void FinalizeLoopStop()
    {
        activeSource.Stop();
        activeSource.volume = GameConstants.Volume.Maximum;
        activeSource = null;

        playing = false;
    }

    private void PerformFadeOutStep()
    {
        float percentage = activeSourceFadeOutTimer.time / activeSourceFadeOutTimer.interval;
        float volume = Mathf.Lerp(
            GameConstants.Volume.Maximum, GameConstants.Volume.Silent, percentage);

        activeSource.volume = volume;
    }

    private void StopLoop()
    {
        if (nextSource != null)
            nextSource.Stop();

        activeSourceFadeOutTimer.Start();
    }

    private void OnLevelLoaded(EventData data)
    {
        bool lastSceneIsMainMenu = lastSceneType == SceneUtility.SceneType.MainMenu;
        if (lastSceneIsMainMenu)
            StopLoop();

        lastSceneType = SceneUtility.SceneType.Level;
    }
}
