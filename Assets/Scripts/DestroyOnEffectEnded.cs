﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOnEffectEnded : MonoBehaviour
{
    [SerializeField]
    private new ParticleSystem particleSystem;

    // Update is called once per frame
    void Update()
    {
        if (particleSystem.isStopped)
            Destroy(gameObject);
    }
}
