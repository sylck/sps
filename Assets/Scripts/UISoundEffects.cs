﻿using UnityEngine;
using UnityEngine.Events;
using UI;
using Inventory.UI;

public class UISoundEffects : MonoBehaviour
{
    [SerializeField]
    private AudioSource audioSource;

    [Header("Inventory")]
    [SerializeField]
    private AudioClip stackPickUp;
    [SerializeField]
    private AudioClip stackPutDown;
    [SerializeField]
    private AudioClip stackSplit;
    [SerializeField]
    private AudioClip stackDestroy;

    [Header("Player")]
    [SerializeField]
    private AudioClip playerDie;
    [SerializeField]
    private AudioClip playerWin;

    [Header("Button")]
    [SerializeField]
    private AudioClip buttonHover;
    [SerializeField]
    private AudioClip buttonClick;

    private EventClipMapEntry[] eventClipMap;

    private class EventClipMapEntry
    {
        private string m_EventName;
        public string eventName { get { return m_EventName; } }

        private UnityAction<EventData> m_Listener;
        public UnityAction<EventData> listener { get { return m_Listener; } }

        private AudioClip m_Clip;
        private AudioSource m_Source;

        public EventClipMapEntry(string eventName, AudioClip clip, UISoundEffects parent)
        {
            m_EventName = eventName;
            m_Clip = clip;

            m_Source = parent.audioSource;
            m_Listener = PlayHandler;
        }

        private void PlayHandler(EventData data)
        {
            m_Source.PlayOneShot(m_Clip);
        }
    }

    private void Awake()
    {
        PopulateEventClipMap();
    }

    private void OnEnable()
    {
        RunOnEachEventClipMapEntry(EventManager.AddListener);
    }

    private void Start()
    {
        audioSource.ignoreListenerPause = true;
        audioSource.ignoreListenerVolume = true;
    }

    private void OnDisable()
    {
        RunOnEachEventClipMapEntry(EventManager.RemoveListener);
    }

    private void PopulateEventClipMap()
    {
        eventClipMap = new EventClipMapEntry[]
        {
            new EventClipMapEntry(TileEvents.StackPickedUp,              stackPickUp,  this),
            new EventClipMapEntry(TileEvents.StackPutDown,               stackPutDown, this),
            new EventClipMapEntry(TileEvents.StackSplit,                 stackSplit,   this),
            new EventClipMapEntry(TileEvents.StackDestroyed,             stackDestroy, this),

            new EventClipMapEntry(GameManagerEvents.EnteredDeathState,   playerDie,    this),
            new EventClipMapEntry(GameManagerEvents.EnteredVictoryState, playerWin,    this),

            new EventClipMapEntry(ButtonEvents.Hovered,                  buttonHover,  this),
            new EventClipMapEntry(ButtonEvents.Clicked,                  buttonClick,  this)
        };
    }

    private void RunOnEachEventClipMapEntry(UnityAction<string, UnityAction<EventData>> 
        eventManagerAction)
    {
        for (int i = 0; i < eventClipMap.Length; i++)
        {
            EventClipMapEntry entry = eventClipMap[i];
            eventManagerAction(entry.eventName, entry.listener);
        }
    }
}
