﻿using UnityEngine;

public class AudioListenerManager : MonoBehaviour
{
    [SerializeField]
    private AudioListener audioListener;

    private AudioListener claimantAudioListener;

    private void OnEnable()
    {
        EventManager.AddListener(LoadManagerEvents.SceneUnloading, OnSceneUnloading);
    }

    private void OnDisable()
    {
        EventManager.RemoveListener(LoadManagerEvents.SceneUnloading, OnSceneUnloading);
    }

    public void Claim(AudioListener claimantAudioListener)
    {
        this.claimantAudioListener = claimantAudioListener;
        claimantAudioListener.enabled = true;
        audioListener.enabled = false;
    }

    private void Reclaim()
    {
        audioListener.enabled = true;

        if (claimantAudioListener == null)
            return;

        claimantAudioListener.enabled = false;
        claimantAudioListener = null;
    }

    private void OnSceneUnloading(EventData data)
    {
        Reclaim();
    }
}
