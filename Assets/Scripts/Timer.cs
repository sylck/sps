﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class Timer
{
    public class Builder
    {
        private bool m_Enabled;
        private bool m_AutoReset;
        private float m_Interval;
        private UnityAction m_Ticked, m_Elapsed;

        public Builder()
        {
            // if not defined, assume:
            // * timer not running initially
            m_Enabled = false;
            // * timer triggers events repeatedly without stopping on m_Time >= m_Interval
            m_AutoReset = true;
            // * timer trigger interval is 0.1 s (100 ms)
            m_Interval = .1f;
        }

        public Builder SetInterval(float interval)
        {
            m_Interval = interval;
            return this;
        }

        public Builder AddTickedListener(UnityAction tickedListener)
        {
            m_Ticked += tickedListener;
            return this;
        }

        public Builder AddElapsedListener(UnityAction elapsedListener)
        {
            m_Elapsed += elapsedListener;
            return this;
        }

        public Builder SetEnabled(bool enabled)
        {
            m_Enabled = enabled;
            return this;
        }

        public Builder SetAutoReset(bool autoReset)
        {
            m_AutoReset = autoReset;
            return this;
        }

        public Timer Build()
        {
            return new Timer(m_Interval, m_Ticked, m_Elapsed, m_Enabled, m_AutoReset);
        }
    }

    private float m_Interval;
    public float interval { get { return m_Interval; } set { m_Interval = value; } }

    private bool m_Enabled;
    public bool enabled { get { return m_Enabled; } }

    private float m_Time;
    public float time { get { return m_Time; } }

    private bool m_AutoReset;
    private UnityAction m_Ticked, m_Elapsed;

    private Timer(
        float interval,
        UnityAction tickedListeners,
        UnityAction elapsedListeners,
        bool enabled,
        bool autoReset)
    {
        m_Interval = interval;
        m_Ticked = tickedListeners;
        m_Elapsed = elapsedListeners;
        m_Enabled = enabled;
        m_AutoReset = autoReset;

        m_Time = 0f;
    }

    public void Tick(float time)
    {
        if (!m_Enabled)
            return;

        m_Time += time;

        UnityActionUtility.Invoke(m_Ticked);

        if (m_Time >= m_Interval)
            Elapsed();
    }

    public void Start()
    {
        // already started, leave
        if (m_Enabled)
            return;

        m_Enabled = true;
        Reset();
    }

    public void Stop()
    {
        if (!m_Enabled)
            return;

        m_Enabled = false;
        Reset();
    }

    private void Elapsed()
    {
        UnityActionUtility.Invoke(m_Elapsed);

        if (m_AutoReset)
            Reset();
        else
            Stop();
    }

    public void Reset()
    {
        m_Time = 0f;
    }
}
