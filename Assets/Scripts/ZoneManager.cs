﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ZoneManager : MonoBehaviour
{
    [SerializeField]
    private int hpDecrement = 5,
                hpIncrement = 5;

    [SerializeField]
    private float damageInterval = .9f;

    private Timer timer;
    private List<Damageable> damageables;
    private List<Healable> healables;
    
    private const DamageType damageType = DamageType.Zone;

    private void Awake()
    {
        timer = new Timer.Builder()
            .SetInterval(damageInterval)
            .AddElapsedListener(TimerElapsed)
            .SetEnabled(true)
            .Build();

        damageables = new List<Damageable>();
        healables = new List<Healable>();
    }

    // Update is called once per frame
    void Update()
    {
        timer.Tick(Time.deltaTime);
    }

    private void TimerElapsed()
    {
        Damage();
        Heal();
    }

    private bool IsValidDamageable(Damageable damageable)
    {
        return damageable != null && 
            damageable.enabled && 
            damageable.SusceptibleTo(damageType);
    }

    private void Damage()
    {
        for (int i = 0; i < damageables.Count; i++)
        {
            Damageable damageable = damageables[i];

            if (!IsValidDamageable(damageable))
            {
                damageables.Remove(damageable);
                i--;
            }
            else
            {
                damageable.TakeDamage(damageType, hpDecrement);
            }
        }
    }

    private void Heal()
    {
        foreach (Healable healable in healables)
            healable.Heal(hpIncrement);
    }

    private void TryAddGameObject(GameObject gameObject, ZoneTile.TileType tileType)
    {
        switch (tileType)
        {
            case ZoneTile.TileType.Damaging:
                TryAddGameObjectDamageable(gameObject);
                break;
            case ZoneTile.TileType.Healing:
                TryAddGameObjectHealable(gameObject);
                break;
        }
    }

    private void TryAddGameObjectDamageable(GameObject gameObject)
    {
        Damageable damageable = gameObject.GetComponent<Damageable>();

        if (!IsValidDamageable(damageable) || 
            damageables.Contains(damageable))
            return;

        damageables.Add(damageable);
    }

    private void TryAddGameObjectHealable(GameObject gameObject)
    {
        Healable healable = gameObject.GetComponent<Healable>();

        if (healable == null 
            || healables.Contains(healable))
            return;

        healables.Add(healable);
    }

    private void TryRemoveGameObject(GameObject gameObject, ZoneTile.TileType tileType)
    {
        switch (tileType)
        {
            case ZoneTile.TileType.Damaging:
                TryRemoveGameObjectDamageable(gameObject);
                break;
            case ZoneTile.TileType.Healing:
                TryRemoveGameObjectHealable(gameObject);
                break;
        }
    }

    private void TryRemoveGameObjectDamageable(GameObject gameObject)
    {
        Damageable damageable = gameObject.GetComponent<Damageable>();

        if (damageable == null)
            return;

        damageables.Remove(damageable);
    }

    private void TryRemoveGameObjectHealable(GameObject gameObject)
    {
        Healable healable = gameObject.GetComponent<Healable>();

        if (healable == null)
            return;

        healables.Remove(healable);
    }

    public void ZoneEnter(GameObject gameObject, ZoneTile.TileType tileType)
    {
        TryAddGameObject(gameObject, tileType);
    }

    public void ZoneStay(GameObject gameObject, ZoneTile.TileType tileType)
    {
        TryAddGameObject(gameObject, tileType);
    }

    public void ZoneExit(GameObject gameObject, ZoneTile.TileType tileType)
    {
        TryRemoveGameObject(gameObject, tileType);
    }
}
