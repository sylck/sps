﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Health : MonoBehaviour
{
    public event UnityAction zeroHpReached;
    public event UnityAction<int> hpChanged, hpIncreased, hpDecreased;
    
    [SerializeField]
    private int m_MaxHealthPoints;
    public int maxHealthPoints { get { return m_MaxHealthPoints; } }

    private int m_HealthPoints;
    public int healthPoints { get { return m_HealthPoints; } }

    private void Awake()
    {
        m_HealthPoints = m_MaxHealthPoints;
    }

    public void AddHp(int amount)
    {
        if (amount <= 0)
            return;

        ApplyHpChange(amount);
    }

    public void SubtractHp(int amount)
    {
        if (amount <= 0)
            return;

        ApplyHpChange(-amount);
    }

    private void ApplyHpChange(int hpDelta)
    {
        int hpBefore = m_HealthPoints,
            hpAfter, hpEffectiveDelta;

        if (hpDelta == 0)
            return;

        m_HealthPoints += hpDelta;

        if (m_HealthPoints > m_MaxHealthPoints)
        {
            m_HealthPoints = m_MaxHealthPoints;
        }
        else if (m_HealthPoints <= 0)
        {
            m_HealthPoints = 0;
            UnityActionUtility.Invoke(zeroHpReached);
        }

        hpAfter = m_HealthPoints;
        hpEffectiveDelta = hpAfter - hpBefore;

        if (hpEffectiveDelta == 0)
            return;

        if (hpEffectiveDelta < 0)
            UnityActionUtility.Invoke(hpDecreased, hpEffectiveDelta);
        else
            UnityActionUtility.Invoke(hpIncreased, Mathf.Abs(hpEffectiveDelta));

        UnityActionUtility.Invoke(hpChanged, hpEffectiveDelta);
    }
}
