﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public static class PickableEvents
{
    public const string Activated = "pickable.activated";
    public const string Picking = "pickable.picking";
    public const string Picked = "pickable.picked";
    public const string Deactivated = "pickable.deactivated";
}

public class Pickable : MonoBehaviour
{
    public event UnityAction<Pickable> picked;

    [SerializeField]
    private ItemData m_ItemData;
    public ItemData itemData { get { return m_ItemData; } }

    [SerializeField]
    private PickMovementEffect pickMovementEffect;
    
    [SerializeField]
    private MeshRenderer meshRenderer;

    [SerializeField]
    private Material glowMaterial;

    private Material normalMaterial;

    private void Start()
    {
        normalMaterial = meshRenderer.material;

        pickMovementEffect.finished += OnPickEffectFinished;
    }

    public void Activate()
    {
        Highlight();
        EventManager.Invoke(PickableEvents.Activated, m_ItemData);
    }

    private void Highlight()
    {
        meshRenderer.material = glowMaterial;
    }

    public void Deactivate()
    {
        Unhighlight();
        EventManager.Invoke(PickableEvents.Deactivated);
    }

    private void Unhighlight()
    {
        meshRenderer.material = normalMaterial;
    }

    public void Pick(Transform pickDestination)
    {
        EventManager.Invoke(PickableEvents.Picking);
        pickMovementEffect.PickTo(pickDestination);
    }

    private void OnPickEffectFinished()
    {
        UnityActionUtility.Invoke(picked, this);
        Destroy(gameObject);
    }
}
