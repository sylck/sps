﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Startup : MonoBehaviour
{
    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    // Start is called before the first frame update
    void Start()
    {
        // don't load Common scene if already loaded
        for (int i = 0; i < SceneManager.sceneCount; i++)
        {
            Scene scene = SceneManager.GetSceneAt(i);
            if (scene.buildIndex == (int)SceneUtility.SceneType.Common)
                return;
        }

        SceneUtility.LoadScene(SceneUtility.SceneType.Common, LoadSceneMode.Additive);
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (scene.buildIndex != (int)SceneUtility.SceneType.Common)
            return;

        LoadManager loadManager = FindObjectOfType<LoadManager>();
        loadManager.LoadScene(SceneUtility.SceneType.MainMenu);
    }
}
