﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public static class LoadManagerEvents
{
    public const string SceneLoading = "loadManager.sceneLoading";
    public const string SceneLoadProgress = "loadManager.sceneLoadProgress";
    public const string SceneLoaded = "loadManager.sceneLoaded";
    public const string SceneUnloading = "loadManager.sceneUnloading";
    public const string SceneUnloadProgress = "loadManager.sceneUnloadProgress";
    public const string SceneUnloaded = "loadManager.sceneUnloaded";
}

public class LoadManager : MonoBehaviour
{
    [SerializeField]
    private EventSystem eventSystem;

    private int activeSceneBuildIndex = SceneUtility.UndefinedSceneBuildIndex,
                pendingSceneBuildIndex;

    private delegate void Callback();

    public void LoadScene(SceneUtility.SceneType sceneType)
    {
        LoadScene((int)sceneType);
    }

    public void ReloadActiveScene()
    {
        LoadScene(activeSceneBuildIndex);
    }

    private void LoadScene(int sceneBuildIndex)
    {
        RaiseSceneLoading(sceneBuildIndex);

        pendingSceneBuildIndex = sceneBuildIndex;

        // if an active scene exists, unload
        if (activeSceneBuildIndex != SceneUtility.UndefinedSceneBuildIndex) 
        {
            Callback callback = StartLoadPendingScene;
            StartUnloadActiveScene(callback);
            return;
        }

        StartLoadPendingScene();
    }

    private void StartUnloadActiveScene(Callback callback)
    {
        StartCoroutine(UnloadActiveScene(callback));
    }

    private IEnumerator UnloadActiveScene(Callback callback)
    {
        RaiseSceneUnloading(activeSceneBuildIndex);

        AsyncOperation unload = SceneManager.UnloadSceneAsync(activeSceneBuildIndex);

        while (!unload.isDone)
        {
            RaiseSceneUnloadProgress(activeSceneBuildIndex, unload.progress);
            yield return null;
        }

        SceneUnloaded(activeSceneBuildIndex);
        activeSceneBuildIndex = SceneUtility.UndefinedSceneBuildIndex;

        callback();
    }

    private void StartLoadPendingScene()
    {
        StartCoroutine(LoadPendingScene());
    }

    private IEnumerator LoadPendingScene()
    {
        AsyncOperation load = SceneManager.LoadSceneAsync(pendingSceneBuildIndex, 
            LoadSceneMode.Additive);

        while (!load.isDone)
        {
            RaiseSceneLoadProgress(pendingSceneBuildIndex, load.progress);
            yield return null;
        }

        // scene loaded, now set it to be the currently active one, otherwise lighting won't look 
        // right
        SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(pendingSceneBuildIndex));

        activeSceneBuildIndex = pendingSceneBuildIndex;
        pendingSceneBuildIndex = SceneUtility.UndefinedSceneBuildIndex;

        RaiseSceneLoaded(activeSceneBuildIndex);
    }

    private void PerformReset()
    {
        // reset previously active selections - e.g. buttons in Menu might stay selected
        eventSystem.SetSelectedGameObject(null);

        // reset timescale if left paused
        Time.timeScale = GameConstants.TimeScale.Realtime;
    }

    private void SceneUnloaded(int sceneBuildIndex)
    {
        PerformReset();     
        RaiseSceneUnloaded(sceneBuildIndex);
    }

    private void RaiseSceneLoading(int sceneBuildIndex)
    {
        EventManager.Invoke(
            EventUtility.TypedEvent(
                LoadManagerEvents.SceneLoading,
                SceneUtility.SceneBuildIndexToType(sceneBuildIndex)));
    }

    private void RaiseSceneLoadProgress(int sceneBuildIndex, float progress)
    {
        EventManager.Invoke(
            EventUtility.TypedEvent(
                LoadManagerEvents.SceneLoadProgress,
                SceneUtility.SceneBuildIndexToType(sceneBuildIndex)),
            progress);
    }

    private void RaiseSceneLoaded(int sceneBuildIndex)
    {
        EventManager.Invoke(
            EventUtility.TypedEvent(
                LoadManagerEvents.SceneLoaded,
                SceneUtility.SceneBuildIndexToType(sceneBuildIndex)));
    }

    private void RaiseSceneUnloading(int sceneBuildIndex)
    {
        EventManager.Invoke(
            LoadManagerEvents.SceneUnloading);

        EventManager.Invoke(
            EventUtility.TypedEvent(
                LoadManagerEvents.SceneUnloading,
                SceneUtility.SceneBuildIndexToType(sceneBuildIndex)));
    }

    private void RaiseSceneUnloadProgress(int sceneBuildIndex, float progress)
    {
        EventManager.Invoke(
            EventUtility.TypedEvent(
                LoadManagerEvents.SceneUnloadProgress,
                SceneUtility.SceneBuildIndexToType(sceneBuildIndex)),
            progress);
    }

    private void RaiseSceneUnloaded(int sceneBuildIndex)
    {
        EventManager.Invoke(
            EventUtility.TypedEvent(
                LoadManagerEvents.SceneUnloaded,
                SceneUtility.SceneBuildIndexToType(sceneBuildIndex)));
    }
}
