﻿using UnityEngine;

public class ObjectiveManager : MonoBehaviour
{
    [SerializeField]
    private GameManager gameManager;

    [SerializeField]
    private Objective[] objectives;

    private void Start()
    {
        for (int i = 0; i < objectives.Length; i++)
            objectives[i].completed += OnCompleted;
    }

    private void OnCompleted()
    {
        // check if all objectives are completed now. if there still are objectives which are not 
        // completed, bail out.
        for (int i = 0; i < objectives.Length; i++)
        {
            if (!objectives[i].IsCompleted())
                return;
        }

        // if we've reached this spot, all objectives are completed
        gameManager.TrySetVictoryState();
    }
}
