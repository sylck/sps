﻿using UnityEngine;

public class SpherePurpleDestroyable : Destroyable
{
    [SerializeField]
    private GameObject explosionPrefab;

    protected override void HandleDestruction()
    {
        GameObject explosion = Instantiate(explosionPrefab);
        explosion.transform.position = gameObject.transform.position;

        DestroySelf();
    }
}
