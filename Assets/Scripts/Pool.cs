﻿using System;
using System.Collections.Generic;
using UnityEngine;

public static class PoolEvents
{
    public const string Initialized = "pool.initialized";
}

public class Pool : MonoBehaviour
{
    [SerializeField]
    private PoolEntry[] poolEntries;

    [Serializable]
    private struct PoolEntry
    {
        public PoolMember prefab;
        public int initialCount;
    }

    private static Pool s_Instance;

    private Dictionary<int, List<GameObject>> pools;

    private void Start()
    {
        pools = new Dictionary<int, List<GameObject>>();

        foreach (PoolEntry poolEntry in poolEntries)
        {
            List<GameObject> pool = new List<GameObject>();
            PoolMember prefab = poolEntry.prefab;

            for (int i = 0; i < poolEntry.initialCount; i++)
            {
                GameObject instance = MakeInstance(prefab);
                instance.SetActive(false);
                pool.Add(instance);
            }

            pools.Add(prefab.GetInstanceID(), pool);
        }

        s_Instance = this;

        EventManager.Invoke(PoolEvents.Initialized);
    }

    private List<GameObject> GetOrMakePool(int prefabId)
    {
        List<GameObject> pool;
        
        if (!pools.TryGetValue(prefabId, out pool))
        {
            pool = new List<GameObject>();
            pools.Add(prefabId, pool);
        }

        return pool;
    }

    private GameObject MakeInstance(PoolMember original)
    {
        GameObject instance = original.MakeInstance();
        instance.transform.parent = transform;
        return instance;
    }

    public static GameObject GetInstance(PoolMember original)
    {
        if (s_Instance == null)
            return null;

        List<GameObject> pool = s_Instance.GetOrMakePool(original.GetInstanceID());
        GameObject instance;

        if (pool.Count > 0)
        {
            instance = pool[pool.Count - 1];
            pool.RemoveAt(pool.Count - 1);
            instance.SetActive(true);
        }
        else
        {
            instance = s_Instance.MakeInstance(original);
        }

        return instance;
    }

    public static bool Reclaim(PoolMember instance)
    {
        if (s_Instance == null)
            return false;
        
        List<GameObject> pool = s_Instance.GetOrMakePool(instance.originalId);

        GameObject instanceGo = instance.gameObject;
        instanceGo.SetActive(false);
        pool.Add(instanceGo);

        return true;
    }
}
