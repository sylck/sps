﻿using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

public class CylinderWhiteStateManager : MonoBehaviour
{
    public event UnityAction dead;

    [SerializeField]
    private Animator animator;

    [SerializeField]
    private Health health;

    [SerializeField]
    private NavMeshAgent navMeshAgent;

    [SerializeField]
    private CylinderWhiteMovement movement;

    private const string k_IdleStateName = "Idle";
    private readonly int k_IdleStateHash = Animator.StringToHash(k_IdleStateName);
    private const string k_DeadStateName = "Dead";
    private readonly int k_DeadStateHash = Animator.StringToHash(k_DeadStateName);

    private const string k_DieTriggerName = "Die";
    private readonly int k_DieTriggerID = Animator.StringToHash(k_DieTriggerName);

    private void OnEnable()
    {
        health.zeroHpReached += OnZeroHpReached;
    }

    private void Update()
    {
        AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo(0);

        if (stateInfo.shortNameHash == k_IdleStateHash)
            OnEnteredIdleState();
        else if (stateInfo.shortNameHash == k_DeadStateHash)
            OnEnteredDeadState();
    }

    private void OnDisable()
    {
        health.zeroHpReached -= OnZeroHpReached;
    }

    private void OnEnteredIdleState()
    {
        // spawn animation finished
        SetComponentsState(true);
    }

    private void OnZeroHpReached()
    {
        SetComponentsState(false);
        animator.SetTrigger(k_DieTriggerID);
    }

    private void OnEnteredDeadState()
    {
        // death animation finished
        UnityActionUtility.Invoke(dead);
        Destroy(gameObject);
    }

    private void SetComponentsState(bool state)
    {
        navMeshAgent.enabled = state;
        movement.enabled = state;
    }
}
