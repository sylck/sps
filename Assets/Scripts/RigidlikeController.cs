﻿using System.Collections.Generic;
using UnityEngine;

public class RigidlikeController : MonoBehaviour
{
    [SerializeField]
    private CharacterController characterController;

    // sound
    [SerializeField]
    private MovementSoundManager movementSoundManager;

    [SerializeField]
    private AudioSource audioSource;

    [SerializeField]
    private AudioClip jumpSound;

    // xz plane values
    [SerializeField]
    private float directionalVelocityMagnitude = 10f,
                  groundDirectionalSensitivity = 1f,
                  airDirectionalSensitivity = .02f,
                  groundFriction = 5f,
                  airFriction = .65f;

    // y axis values
    [SerializeField]
    private float groundedYVelocityThreshold = .02f,
                  jumpVelocity = 5f;

    // rigidbody-like properties
    [SerializeField]
    private float mass = 100f,
                  maxForceMagnitudePerMass = 54f;

    private bool grounded, jumping;
    private Vector3 velocity;
    private float timeSinceLastJumpSound;

    // inputs
    private Vector3 worldSpaceDirectionalInput;
    private bool jumpInput;

    // trigger tracking
    private List<Collider> triggers;
    private const string k_OnTriggerEnter = "OnTriggerEnter",
                         k_OnTriggerStay = "OnTriggerStay",
                         k_OnTriggerExit = "OnTriggerExit";

    private void Start()
    {
        triggers = new List<Collider>();
        characterController.detectCollisions = false;

        movementSoundManager.SetMaxVelocityMagnitude(directionalVelocityMagnitude);
    }

    // Update is called once per frame
    void Update()
    {
        UpdateGroundedState();
        ResetYVelocity();
        HandleWorldInteraction();
        HandleExternalVelocityInput();
        InteractWithWorld();
        Move();
        UpdateSound();
        UpdateTriggers();
        ApplyMotionResistance();
    }

    private bool GetObjectBelow(out RaycastHit hitInfo)
    {
        // objects very close to skin sometimes do not get captured, cast a smaller box
        Vector3 extent = transform.localScale;
        extent.y -= characterController.radius;

        float maxDistance = characterController.radius / 2f + characterController.skinWidth;

        return Physics.BoxCast(transform.position, extent / 2f, Vector3.down, out hitInfo,
            transform.rotation, maxDistance, Physics.DefaultRaycastLayers,
            QueryTriggerInteraction.Ignore);
    }

    private void UpdateGroundedState()
    {
        // start with assuming grounded is false
        grounded = false;

        RaycastHit hitInfo;
        bool objectFound = GetObjectBelow(out hitInfo);

        // nothing's below
        if (!objectFound)
            return;

        Rigidbody rigidbody = hitInfo.rigidbody;
        // object below is a collider without rigidbody - stationary enough
        if (rigidbody == null)
        {
            grounded = true;
            return;
        }

        // object below has a rigidbody - is it stationary enough?
        if (Mathf.Abs(rigidbody.velocity.y) < groundedYVelocityThreshold)
        {
            grounded = true;
        }
    }

    private void ResetYVelocity()
    {
        if (grounded)
            velocity.y = 0f;
    }

    private bool WillBeImpactedBy(Rigidbody otherRigidbody)
    {
        RaycastHit hitInfo;
        Vector3 orbVelocity = otherRigidbody.velocity;
        bool sweepFound = otherRigidbody.SweepTest(orbVelocity.normalized, out hitInfo,
            orbVelocity.magnitude, QueryTriggerInteraction.Ignore);

        return sweepFound && hitInfo.collider == characterController;
    }

    private void CollideWith(Rigidbody otherRigidbody)
    {
        // body 1 - this
        // body 2 - other object (rigidbody)

        // do nothing if motion has a significant downward part
        Vector3 direction = velocity.normalized;
        if (direction.y < -.3f)
            return;

        // do nothing if otherRigidbody is below this one
        RaycastHit hitInfo;
        GetObjectBelow(out hitInfo);
        if (hitInfo.rigidbody == otherRigidbody)
            return;

        Vector3 v1i = velocity;
        Vector3 v2i = otherRigidbody.velocity;
        Vector3 x1 = transform.position;
        Vector3 x2 = otherRigidbody.position;
        float m1 = mass;
        float m2 = otherRigidbody.mass;

        // based on values provided here:
        // https://en.wikipedia.org/wiki/Elastic_collision
        Vector3 v1f = v1i - ((2f * m2) / (m1 + m2)) *
            Vector3.Dot(v1i - v2i, x1 - x2) / (x1 - x2).sqrMagnitude * (x1 - x2);
        Vector3 v2f = v2i - ((2f * m1) / (m1 + m2)) *
            Vector3.Dot(v2i - v1i, x2 - x1) / (x2 - x1).sqrMagnitude * (x2 - x1);

        // copy original (initial) Y velocity values
        v1f.y = v1i.y;
        v2f.y = v2i.y;

        otherRigidbody.velocity = v2f;
        velocity = v1f;
    }

    private void HandleCollider(Collider collider, bool shouldCheckImpact)
    {
        if (collider == characterController)
            return;

        Rigidbody otherRigidbody = collider.attachedRigidbody;

        if (otherRigidbody == null || otherRigidbody.isKinematic)
            return;

        if (shouldCheckImpact && !WillBeImpactedBy(otherRigidbody))
            return;

        CollideWith(otherRigidbody);
    }

    private void HandleWorldInteraction()
    {
        Collider[] colliders = Physics.OverlapBox(transform.position, transform.localScale,
            transform.rotation, Physics.DefaultRaycastLayers, QueryTriggerInteraction.Ignore);

        for (int i = 0; i < colliders.Length; i++)
        {
            HandleCollider(colliders[i], true);
        }
    }

    private void HandleExternalVelocityInput()
    {
        Vector3 xzVelocityDelta,
                yVelocityDelta;

        Vector3 xzVelocity = new Vector3(velocity.x, 0, velocity.z);
        Vector3 yVelocity = new Vector3(0, velocity.y, 0);

        // xz velocity
        Vector3 directionalInput = worldSpaceDirectionalInput;
        float directionalSensitivity = grounded ?
                groundDirectionalSensitivity :
                airDirectionalSensitivity;

        xzVelocityDelta = directionalInput * directionalSensitivity *
            directionalVelocityMagnitude;

        // cap force exerted by this body to F_max
        // F = m * a = m * dv / dt 
        //  => F_max = m * dv_max / dt
        //  => dv_max / dt = F_max / m
        // 
        // where:
        //  F_max = k * m, 
        //  k = maxForceMagnitudePerMass
        // 
        // dv_max / dt = k * m / m 
        //  => dv_max / dt = k 
        //  => dv_max = k * dt
        float maxXzVelocityDeltaMagnitude = maxForceMagnitudePerMass * Time.deltaTime;

        xzVelocityDelta = Vector3.ClampMagnitude(xzVelocityDelta, maxXzVelocityDeltaMagnitude);
        xzVelocity += xzVelocityDelta;

        // y velocity
        yVelocityDelta = Vector3.zero;

        // can attempt jumping only if grounded
        if (grounded && jumpInput)
        {
            jumping = true;
            yVelocityDelta.y = jumpVelocity;
        }
        else
        {
            jumping = false;
        }

        yVelocity += yVelocityDelta;

        velocity = xzVelocity + yVelocity;

        // reset directional and jump input values
        worldSpaceDirectionalInput = Vector3.zero;
        jumpInput = false;
    }

    private void InteractWithWorld()
    {
        Vector3 extents = transform.localScale;

        Vector3 direction = velocity.normalized;
        float distance = velocity.magnitude * Time.deltaTime;

        RaycastHit[] raycastHits = Physics.BoxCastAll(transform.position, extents / 2f,
            direction, transform.rotation, distance, Physics.DefaultRaycastLayers,
            QueryTriggerInteraction.Ignore);

        for (int i = 0; i < raycastHits.Length; i++)
        {
            HandleCollider(raycastHits[i].collider, false);
        }
    }

    private void Move()
    {
        characterController.Move(velocity * Time.deltaTime);
    }

    private void TryPlayJumpSound()
    {
        timeSinceLastJumpSound += Time.deltaTime;
      
        if (!jumping || timeSinceLastJumpSound < jumpSound.length)
            return;

        audioSource.PlayOneShot(jumpSound);
        timeSinceLastJumpSound = 0f;
    }

    private void UpdateSound()
    {
        movementSoundManager.SetVelocity(velocity);
        TryPlayJumpSound();
    }

    private void UpdateTriggers()
    {
        Vector3 mesh = transform.localScale;
        Vector3 skin = Vector3.one * characterController.skinWidth * 2f;
        Vector3 extents = mesh + skin;

        Collider[] colliders = Physics.OverlapBox(transform.position, extents / 2f,
            transform.rotation, Physics.DefaultRaycastLayers, QueryTriggerInteraction.Collide);

        List<Collider> oldTriggers = new List<Collider>(triggers);

        for (int i = 0; i < colliders.Length; i++)
        {
            Collider collider = colliders[i];
            if (!collider.isTrigger)
                continue;

            string message;
            int index = oldTriggers.IndexOf(collider);

            if (index == -1)
            {
                // OnTriggerEnter
                message = k_OnTriggerEnter;
                triggers.Add(collider);
            }
            else
            {
                // OnTriggerStay
                message = k_OnTriggerStay;
                oldTriggers.RemoveAt(index);
            }

            collider.SendMessage(message, characterController);
        }

        // check for leftover triggers in oldTriggers for OnTriggerExit
        for (int i = 0; i < oldTriggers.Count; i++)
        {
            Collider trigger = oldTriggers[i];
            trigger.SendMessage(k_OnTriggerExit, characterController);
            triggers.Remove(trigger);
        }
    }

    private void ApplyMotionResistance()
    {
        Vector3 xzVelocity = new Vector3(velocity.x, 0, velocity.z),
                yVelocity = new Vector3(0, velocity.y, 0);

        float friction = grounded ? groundFriction : airFriction;
        xzVelocity = Vector3.Lerp(xzVelocity, Vector3.zero, friction * Time.deltaTime);

        if (!grounded && !jumping)
            yVelocity.y += Physics.gravity.y * Time.deltaTime;

        velocity = xzVelocity + yVelocity;
    }

    public Vector3 CompensateForMovement(Vector3 point)
    {
        Vector3 displacement = velocity * Time.deltaTime;
        return point + displacement;
    }

    public void SetWorldSpaceDirectionalInput(Vector3 input)
    {
        worldSpaceDirectionalInput = input;
    }

    public void SetJumpInput(bool input)
    {
        jumpInput = input;
    }
}
