﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField]
    private Transform cameraTransform;

    [SerializeField]
    private RigidlikeController controller;

    // mouse look
    [SerializeField]
    private float mouseLookSensitivity = 12f,
                  cameraAngleLimit = 60f;

    private float cameraXAngle;

    // Update is called once per frame
    void Update()
    {
        // handle position
        HandleKeyboardInput();

        // handle rotation
        HandleMouseInput();
    }

    private void HandleKeyboardInput()
    {
        controller.SetWorldSpaceDirectionalInput(GetWorldSpaceDirectionalInput());
        controller.SetJumpInput(GetJumpInput());
    }

    private Vector3 GetWorldSpaceDirectionalInput()
    {
        float horizontal = 0;
        float vertical = 0;

        if (Input.GetKey(KeyCode.W))
            vertical += 1;
        if (Input.GetKey(KeyCode.S))
            vertical -= 1;

        if (Input.GetKey(KeyCode.A))
            horizontal -= 1;
        if (Input.GetKey(KeyCode.D))
            horizontal += 1;

        Vector3 localDirectionalInput = new Vector3(horizontal, 0, vertical);
        return transform.TransformVector(localDirectionalInput).normalized;
    }

    private bool GetJumpInput()
    {
        return Input.GetKey(KeyCode.Space);
    }

    private void HandleMouseInput()
    {
        HandleMouseX();
        HandleMouseY();
    }

    private void HandleMouseX()
    {
        float mouseDeltaX = Input.GetAxis("Mouse X");

        float playerMeshYRotation = mouseDeltaX * mouseLookSensitivity;
        transform.Rotate(0, playerMeshYRotation, 0);
    }

    private void HandleMouseY()
    {
        float mouseDeltaY = Input.GetAxis("Mouse Y");

        // -mouseDeltaY because otherwise camera goes down when mouse goes up and vice versa
        float cameraXAngleDelta = -mouseDeltaY * mouseLookSensitivity;

        cameraXAngle += cameraXAngleDelta;

        // cap camera rotation so that it doesn't flip
        float minCameraXAngle = -cameraAngleLimit;
        float maxCameraXAngle = cameraAngleLimit;

        cameraXAngle = Mathf.Clamp(cameraXAngle, minCameraXAngle, maxCameraXAngle);

        cameraTransform.localRotation = Quaternion.Euler(cameraXAngle, 0, 0);
    }
}
