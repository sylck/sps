﻿using UnityEngine;

public class MovementSoundManager : MonoBehaviour
{
    [SerializeField]
    private AudioSource loopAudioSource;

    [SerializeField]
    private float jumpSensitivity = 5f;

    private float maxVelocityMagnitude;

    private const float k_VolumeStopThreshold = .1f,
                        k_VolumePlayThreshold = .3f;

    public void SetMaxVelocityMagnitude(float maxVelocityMagnitude)
    {
        this.maxVelocityMagnitude = maxVelocityMagnitude;
    }

    private void TryPlay()
    {
        if (loopAudioSource.isPlaying)
            return;

        loopAudioSource.Play();
    }

    private void TryStop()
    {
        if (!loopAudioSource.isPlaying)
            return;

        loopAudioSource.Stop();
    }

    public void SetVelocity(Vector3 velocity)
    {
        Vector3 xzVelocity = new Vector3(velocity.x, 0f, velocity.z);
        float volume = xzVelocity.magnitude / maxVelocityMagnitude;

        if (Mathf.Abs(velocity.y) > 0f)
            volume = Mathf.Lerp(loopAudioSource.volume, 0f, jumpSensitivity * Time.deltaTime);

        loopAudioSource.volume = volume;

        if (volume > k_VolumePlayThreshold)
            TryPlay();
        else if (volume < k_VolumeStopThreshold)
            TryStop();
    }
}
