﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class UnityActionUtility
{
    public static void Invoke(UnityAction action)
    {
        if (action != null)
            action.Invoke();
    }

    public static void Invoke<T0>(UnityAction<T0> action, T0 arg0)
    {
        if (action != null)
            action.Invoke(arg0);
    }

    public static void Invoke<T0, T1>(UnityAction<T0, T1> action, T0 arg0, T1 arg1)
    {
        if (action != null)
            action.Invoke(arg0, arg1);
    }
}
