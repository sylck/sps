﻿using UnityEngine;
using UnityEngine.Events;

public abstract class Objective : MonoBehaviour
{
    public event UnityAction completed;

    public abstract bool IsCompleted();

    protected void TryRaiseCompleted()
    {
        if (!IsCompleted())
            return;

        RaiseCompleted();
    }

    private void RaiseCompleted()
    {
        UnityActionUtility.Invoke(completed);
    }
}
