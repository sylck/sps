﻿using UnityEngine;
using UnityEngine.Events;

namespace Inventory
{
    public class Grid
    {
        private const int k_Columns = 6;
        public int columns { get { return k_Columns; } }

        private const int k_Rows = 4;
        public int rows { get { return k_Rows; } }

        private int m_SlotCount;
        public int slotCount { get { return m_SlotCount; } }

        private Stack[] slots;

        public Grid(UnityAction<Stack> destroyedListeners)
        {
            m_SlotCount = k_Columns * k_Rows;
            slots = new Stack[m_SlotCount];

            for (int i = 0; i < m_SlotCount; i++)
            {
                Stack stack = new Stack();
                stack.grid = this;
                stack.destroyed += destroyedListeners;
                slots[i] = stack;
            }
        }

        private bool CheckCanRemoveItems(ThingType item, int count)
        {
            int remainingAmountToRemove = count;

            for (int i = 0; i < m_SlotCount; i++)
            {
                Stack stack = slots[i];

                if (stack.itemType != item)
                    continue;

                remainingAmountToRemove -= stack.itemCount;

                if (remainingAmountToRemove < 0)
                    remainingAmountToRemove = 0;

                if (remainingAmountToRemove == 0)
                    break;
            }

            return remainingAmountToRemove == 0;
        }

        private void RemoveItems(ThingType item, int count)
        {
            int remainingAmountToRemove = count;

            for (int i = 0; i < m_SlotCount; i++)
            {
                Stack stack = slots[i];

                if (stack.itemType != item)
                    continue;

                int amountToRemove = Mathf.Min(remainingAmountToRemove, stack.itemCount);
                stack.Remove(amountToRemove);

                remainingAmountToRemove -= amountToRemove;

                if (remainingAmountToRemove == 0)
                    break;
            }
        }

        public bool TryRemoveItems(ThingType item, int count)
        {
            bool canRemoveItems = CheckCanRemoveItems(item, count);

            if (canRemoveItems)
            {
                RemoveItems(item, count);
            }

            return canRemoveItems;
        }

        private bool StackUnsuitableToAddItem(Stack stack, ThingType item)
        {
            // stack.itemType being different from `item` does not necessarily mean the stack is 
            // incompatible with `item` type. it might be empty, so check that too.
            return stack.IsFull() || (stack.itemType != item && !stack.IsEmpty());
        }

        public bool CheckCanAddItems(ThingType item, int count, ItemData data)
        {
            int remainingAmountToAdd = count;

            for (int i = 0; i < m_SlotCount; i++)
            {
                Stack stack = slots[i];

                if (StackUnsuitableToAddItem(stack, item))
                    continue;

                int freeSpace;
                if (stack.IsEmpty())
                    freeSpace = data.maxStackSize;
                else
                    freeSpace = stack.FreeSpace();

                remainingAmountToAdd -= freeSpace;

                if (remainingAmountToAdd < 0)
                    remainingAmountToAdd = 0;

                if (remainingAmountToAdd == 0)
                    break;
            }

            return remainingAmountToAdd == 0;
        }

        private void AddItems(ThingType item, int count, ItemData data)
        {
            int remainingAmountToAdd = count;

            for (int i = 0; i < m_SlotCount; i++)
            {
                Stack stack = slots[i];

                if (StackUnsuitableToAddItem(stack, item))
                    continue;

                int freeSpace;
                if (stack.IsEmpty())
                    freeSpace = data.maxStackSize;
                else
                    freeSpace = stack.FreeSpace();

                int amountToAdd = Mathf.Min(remainingAmountToAdd, freeSpace);
                if (stack.IsEmpty())
                    stack.Store(item, amountToAdd, data);
                else
                    stack.Add(amountToAdd);

                remainingAmountToAdd -= amountToAdd;

                if (remainingAmountToAdd == 0)
                    break;
            }
        }

        public bool TryAddItems(ThingType item, int count, ItemData data)
        {
            bool canStoreItems = CheckCanAddItems(item, count, data);

            if (canStoreItems)
            {
                AddItems(item, count, data);
            }

            return canStoreItems;
        }

        public bool TryStore(ThingType item, int count, ItemData data)
        {
            for (int i = 0; i < m_SlotCount; i++)
            {
                Stack stack = slots[i];

                if (stack.IsEmpty())
                {
                    stack.Store(item, count, data);
                    return true;
                }
            }

            return false;
        }

        public Stack GetSlot(int x, int y)
        {
            return slots[x + k_Columns * y];
        }
    }
}
