﻿using UnityEngine;
using UnityEngine.Events;

namespace Inventory
{
    public class Stack
    {
        public Grid grid;
        public event UnityAction modified;
        public event UnityAction<Stack> destroyed;

        private ThingType m_ItemType;
        public ThingType itemType { get { return m_ItemType; } }

        private int m_ItemCount;
        public int itemCount { get { return m_ItemCount; } }

        private ItemData m_Data;
        public ItemData data { get { return m_Data; } }

        public Stack()
        {
            Reset();
        }

        public Stack(Stack stack)
        {
            Copy(stack);
        }

        public bool IsEmpty()
        {
            return m_ItemType == ThingType.Undefined;
        }

        public bool IsFull()
        {
            if (m_Data == null)
                return false;

            return m_ItemCount == m_Data.maxStackSize;
        }

        public int FreeSpace()
        {
            if (m_Data == null)
                return -1;

            return m_Data.maxStackSize - itemCount;
        }

        public Sprite GetIcon()
        {
            if (m_Data == null)
                return null;

            return m_Data.icon;
        }

        public bool IsStackable()
        {
            if (m_Data == null)
                return false;

            return m_Data.maxStackSize > 1;
        }

        public void Remove(int count)
        {
            m_ItemCount -= count;

            if (m_ItemCount == 0)
                Reset();

            UnityActionUtility.Invoke(modified);
        }

        public void Store(ThingType item, int count, ItemData data)
        {
            m_ItemType = item;
            m_ItemCount = count;
            m_Data = data;

            UnityActionUtility.Invoke(modified);
        }

        public void Add(int count)
        {
            m_ItemCount += count;

            UnityActionUtility.Invoke(modified);
        }

        public void Destroy()
        {
            // make a copy for `destroyed` event
            Stack destroyedStack = new Stack(this);

            Reset();

            UnityActionUtility.Invoke(destroyed, destroyedStack);
            UnityActionUtility.Invoke(modified);
        }

        public void InteractWith(Stack otherStack)
        {
            if (otherStack.itemType == m_ItemType)
                MergeWith(otherStack);
            else
                SwapWith(otherStack);
        }

        private void MergeWith(Stack otherStack)
        {
            if (otherStack.IsFull())
                return;

            int amountTransferred = Mathf.Min(m_ItemCount, otherStack.FreeSpace());
            otherStack.Add(amountTransferred);
            Remove(amountTransferred);
        }

        private void SwapWith(Stack otherStack)
        {
            Stack tmp;

            // store data from `this` to tmp
            tmp = new Stack(this);

            // store data from otherStack to `this`
            Copy(otherStack);

            // store data from tmp to otherStack
            otherStack.Copy(tmp);

            UnityActionUtility.Invoke(modified);
            UnityActionUtility.Invoke(otherStack.modified);
        }

        public bool Split()
        {
            if (m_ItemCount < 2)
                return false;

            int amountTransferred = m_ItemCount / 2;

            if (grid.TryStore(m_ItemType, amountTransferred, m_Data))
            {
                Remove(amountTransferred);
                return true;
            }
            else
            {
                return false;
            }
        }

        private void Copy(Stack source)
        {
            m_ItemType = source.m_ItemType;
            m_ItemCount = source.m_ItemCount;
            m_Data = source.m_Data;
        }

        private void Reset()
        {
            m_ItemCount = -1;
            m_ItemType = ThingType.Undefined;
            m_Data = null;
        }
    }
}
