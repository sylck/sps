﻿using System.Collections.Generic;

namespace Inventory
{
    public class Stats
    {
        private Dictionary<ThingType, int> m_Stats;
        public Dictionary<ThingType, int> stats { get { return m_Stats; } }

        private readonly ThingType[] k_ItemTypes =
        {
            ThingType.CubeGreen,
            ThingType.CubeYellow,
            ThingType.SphereOrange,
            ThingType.SpherePurple
        };
        private const int k_InitialItemCount = 0;

        public Stats()
        {
            m_Stats = new Dictionary<ThingType, int>();

            foreach (ThingType itemType in k_ItemTypes)
            {
                m_Stats.Add(itemType, k_InitialItemCount);
            }
        }

        public int GetItemCount(ThingType type)
        {
            int count;

            if (!m_Stats.TryGetValue(type, out count))
            {
                m_Stats.Add(type, k_InitialItemCount);
                count = k_InitialItemCount;
            }

            return count;
        }

        private void SetItemCount(ThingType type, int count)
        {
            if (m_Stats.ContainsKey(type))
                m_Stats[type] = count;
            else
                m_Stats.Add(type, count);
        }

        public void ApplyChange(ThingType type, int delta)
        {
            int count = GetItemCount(type);
            count += delta;
            SetItemCount(type, count);
        }
    }
}
