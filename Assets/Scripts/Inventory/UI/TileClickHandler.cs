﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Inventory.UI
{
    public class TileClickHandler : MonoBehaviour, IPointerClickHandler
    {
        [SerializeField]
        private Tile tile;

        public void OnPointerClick(PointerEventData eventData)
        {
            bool split = false;

            bool holdingShift = Input.GetKey(KeyCode.LeftShift) || 
                Input.GetKey(KeyCode.RightShift);
            bool rmbPressed = eventData.button == PointerEventData.InputButton.Right;

            if (holdingShift && rmbPressed)
                split = tile.stack.Split();

            if (split)
                EventManager.Invoke(TileEvents.StackSplit);
        }
    }
}
