﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Inventory.UI
{
    public class TileDragHandler : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
    {
        [NonSerialized]
        public DragIndicator dragIndicator;

        [SerializeField]
        private Tile tile;

        private PointerEventData pointerData;

        private Tile FindTile(GameObject gameObject)
        {
            return gameObject.GetComponentInParent<Tile>();
        }

        private void TerminateDrag()
        {
            pointerData.dragging = false;
            pointerData.pointerDrag = null;
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            pointerData = eventData;

            if (pointerData.button != PointerEventData.InputButton.Left)
            {
                TerminateDrag();
                return;
            }

            dragIndicator.Show(tile.stack.GetIcon());
            EventManager.Invoke(TileEvents.StackPickedUp);
        }

        public void OnDrag(PointerEventData eventData)
        {
            Vector2 mousePosition = pointerData.position;

            dragIndicator.MoveTo(mousePosition);

            if (!tile.uiGrid.IsWithinGridArea(mousePosition))
                dragIndicator.ShowDestroyOverlayIcon();
            else
                dragIndicator.HideDestroyOverlayIcon();
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            Tile tileBelow = FindTile(pointerData.pointerEnter);
            bool destroyed = false;

            // if void below, destroy if out of grid area
            if (tileBelow == null)
            {
                if (!tile.uiGrid.IsWithinGridArea(pointerData.position))
                {
                    tile.stack.Destroy();
                    destroyed = true;
                }
            }
            else if (tileBelow != tile)
            {
                tile.stack.InteractWith(tileBelow.stack);
            }

            if (destroyed)
                EventManager.Invoke(TileEvents.StackDestroyed);
            else
                EventManager.Invoke(TileEvents.StackPutDown);

            dragIndicator.Hide();
        }

        public void OnInventoryDialogHiding()
        {
            // if inventory is hiding and player was still dragging, reset drag state manually
            if (pointerData == null || !pointerData.dragging)
                return;

            TerminateDrag();

            EventManager.Invoke(TileEvents.StackPutDown);
            dragIndicator.Hide();
        }
    }
}
