﻿using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;
using UnityEngine.Events;

namespace Inventory.UI
{
    public static class InventoryDialogEvents
    {
        public const string Showing = "inventory.ui.inventoryDialog.showing";
        public const string Hidden = "inventory.ui.inventoryDialog.hidden";
    }

    public class InventoryDialog : MonoBehaviour, IDialog
    {
        [SerializeField]
        private DialogHandle dialogHandle;

        [SerializeField]
        private GameObject container;

        [SerializeField]
        private Grid uiGrid;

        public event UnityAction hiding;

        private void OnEnable()
        {
            EventManager.AddListener(
                PlayerInventoryEvents.GridInitialized, 
                OnPlayerInventoryGridInitialized);
        }

        private void OnDisable()
        {
            EventManager.RemoveListener(
                PlayerInventoryEvents.GridInitialized, 
                OnPlayerInventoryGridInitialized);
        }

        public void OnClose()
        {
            dialogHandle.Close();
        }

        private void OnPlayerInventoryGridInitialized(EventData data)
        {
            Inventory.Grid playerGrid = data.obj as Inventory.Grid;

            uiGrid.Initialize(playerGrid);
        }

        public void Show()
        {
            EventManager.Invoke(InventoryDialogEvents.Showing);
            GameObjectUtility.Show(container);
        }

        public void Hide()
        {
            UnityActionUtility.Invoke(hiding);
            GameObjectUtility.Hide(container);
            EventManager.Invoke(InventoryDialogEvents.Hidden);
        }
    }
}
