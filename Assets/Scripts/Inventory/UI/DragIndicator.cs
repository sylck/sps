﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UI;

namespace Inventory.UI
{
    public static class DragIndicatorEvents
    {
        public const string Showing = "inventory.ui.dragIndicator.showing";
        public const string Hidden = "inventory.ui.dragIndicator.hidden";
    }

    public class DragIndicator : MonoBehaviour
    {
        [SerializeField]
        private GameObject container, destroyOverlayIcon;

        [SerializeField]
        private Image iconImage;

        [SerializeField]
        private RectTransform containerTransform, parentTransform;

        [SerializeField]
        private CanvasCameraProvider canvasCameraProvider;

        private Sprite m_Icon;

        public void Show(Sprite icon)
        {
            EventManager.Invoke(DragIndicatorEvents.Showing);

            m_Icon = icon;
            iconImage.sprite = m_Icon;
            GameObjectUtility.Show(container);
        }

        public void MoveTo(Vector2 screenPosition)
        {
            Vector2 newPosition;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(parentTransform, 
                screenPosition, canvasCameraProvider.GetCamera(), out newPosition);
            containerTransform.anchoredPosition = newPosition;
        }

        public void ShowDestroyOverlayIcon()
        {
            GameObjectUtility.Show(destroyOverlayIcon);
        }

        public void HideDestroyOverlayIcon()
        {
            GameObjectUtility.Hide(destroyOverlayIcon);
        }

        public void Hide()
        {
            GameObjectUtility.Hide(container);
            GameObjectUtility.Hide(destroyOverlayIcon);

            EventManager.Invoke(DragIndicatorEvents.Hidden);
        }
    }
}
