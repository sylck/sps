﻿using UnityEngine;
using UI;

namespace Inventory.UI
{
    public class Grid : MonoBehaviour
    {       
        [SerializeField]
        private RectTransform tilePrefab;

        [SerializeField]
        private InventoryDialog inventoryDialog;

        [SerializeField]
        private RectTransform gridArea;

        [SerializeField]
        private DragIndicator dragIndicator;

        [SerializeField]
        private CanvasCameraProvider canvasCameraProvider;

        private Tile[] tiles;

        private float CalculateTileXPosition(int gridX, float tileWidth, float gapSize)
        {
            return (1 + gridX * 2) * (tileWidth / 2) + (gridX + 1) * gapSize;
        }

        private float CalculateTileYPosition(int gridY, float tileHeight, float gapSize)
        {
            return -CalculateTileXPosition(gridY, tileHeight, gapSize);
        }

        public void Initialize(Inventory.Grid playerGrid)
        {
            if (tiles == null)
                CreateGrid(playerGrid);
            else
                UpdateBind(playerGrid);
        }

        private void CreateGrid(Inventory.Grid playerGrid)
        {
            Transform gridAreaTransform = transform;

            int pgSlotCount = playerGrid.slotCount;
            int pgColumns = playerGrid.columns;

            tiles = new Tile[pgSlotCount];

            float gridUIWidth = gridArea.rect.width;

            float tileWidth = tilePrefab.rect.width;
            float tileHeight = tilePrefab.rect.height;

            float gapSize = (gridUIWidth - pgColumns * tileWidth) / (pgColumns + 1);

            for (int i = 0; i < pgSlotCount; i++)
            {
                int gridX = i % pgColumns;
                int gridY = i / pgColumns;

                RectTransform tileTransform = Instantiate(tilePrefab, gridAreaTransform);

                tileTransform.anchoredPosition = new Vector3(
                    CalculateTileXPosition(gridX, tileWidth, gapSize),
                    CalculateTileYPosition(gridY, tileHeight, gapSize));

                Tile tile = tileTransform.GetComponent<Tile>();
                tiles[i] = tile;

                tile.uiGrid = this;
                tile.dragHandler.dragIndicator = dragIndicator;
                inventoryDialog.hiding += tile.dragHandler.OnInventoryDialogHiding;
                tile.Bind(playerGrid.GetSlot(gridX, gridY));
            }
        }

        private void UpdateBind(Inventory.Grid playerGrid)
        {
            int pgSlotCount = playerGrid.slotCount;
            int pgColumns = playerGrid.columns;

            for (int i = 0; i < pgSlotCount; i++)
            {
                int gridX = i % pgColumns;
                int gridY = i / pgColumns;

                Tile tile = tiles[i];
                tile.Bind(playerGrid.GetSlot(gridX, gridY));
            }
        }

        public bool IsWithinGridArea(Vector2 point)
        {
            return RectTransformUtility.RectangleContainsScreenPoint(gridArea, point, 
                canvasCameraProvider.GetCamera());
        }
    }
}
