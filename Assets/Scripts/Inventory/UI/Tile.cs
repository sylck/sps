﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Inventory.UI
{
    public static class TileEvents
    {
        public const string StackPickedUp = "inventory.ui.tile.stackPickedUp";
        public const string StackPutDown = "inventory.ui.tile.stackPutDown";
        public const string StackSplit = "inventory.ui.tile.stackSplit";
        public const string StackDestroyed = "inventory.ui.tile.stackDestroyed";
    }

    public class Tile : MonoBehaviour
    {
        [NonSerialized]
        public Grid uiGrid;

        [SerializeField]
        private Sprite emptyStackIcon;

        [SerializeField]
        private Image iconLabel;

        [SerializeField]
        private Text countLabel;

        [SerializeField]
        private TileDragHandler m_DragHandler;
        public TileDragHandler dragHandler { get { return m_DragHandler; } }

        [SerializeField]
        private TileClickHandler clickHandler;

        private Stack m_Stack;
        public Stack stack { get { return m_Stack; } }

        private const int k_MaxPrintableItemCount = 999;

        public void Bind(Stack stack)
        {
            m_Stack = stack;
            m_Stack.modified += OnStackModified;

            Refresh();
        }

        private void Refresh()
        {
            RefreshIcon();
            RefreshCount();
            UpdateMouseHandlersState();
        }

        private void RefreshIcon()
        {
            if (m_Stack.IsEmpty())
                iconLabel.sprite = emptyStackIcon;
            else
                iconLabel.sprite = m_Stack.GetIcon();
        }

        private void RefreshCount()
        {
            countLabel.text = MakeCountText(m_Stack.itemCount);
        }

        private void UpdateMouseHandlersState()
        {
            // prevent splitting and dragging of an empty stack
            if (m_Stack.IsEmpty())
            {
                m_DragHandler.enabled = false;
                clickHandler.enabled = false;
            }
            else
            {
                m_DragHandler.enabled = true;
                clickHandler.enabled = true;
            }
        }

        private string MakeCountText(int count)
        {
            string text;

            if (!m_Stack.IsStackable() || m_Stack.IsEmpty())
                text = "";
            else if (count > k_MaxPrintableItemCount)
                text = k_MaxPrintableItemCount + "+";
            else
                text = count.ToString();

            return text;
        }

        private void OnStackModified()
        {
            Refresh();
        }
    }
}
