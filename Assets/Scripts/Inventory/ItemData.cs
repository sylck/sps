﻿using UnityEngine;

[CreateAssetMenu]
public class ItemData : ScriptableObject
{
    [SerializeField]
    private string m_DisplayName;
    public string displayName { get { return m_DisplayName; } }

    [SerializeField]
    private Sprite m_Icon;
    public Sprite icon { get { return m_Icon; } }

    [SerializeField]
    private int m_MaxStackSize;
    public int maxStackSize { get { return m_MaxStackSize; } }
}
