﻿using UnityEngine;
using UnityEngine.Events;

public class PickMovementEffect : MonoBehaviour
{
    public event UnityAction finished;

    [SerializeField]
    private new Rigidbody rigidbody;

    [SerializeField]
    private Collider collider;

    [SerializeField]
    private new Transform transform;

    [SerializeField]
    private float effectTime;

    private Timer effectTimer;
    private Vector3 initialPosition;
    private Transform target;

    private void Start()
    {
        effectTimer = new Timer.Builder()
            .SetInterval(effectTime)
            .SetAutoReset(false)
            .AddTickedListener(LerpPosition)
            .AddElapsedListener(EffectEnded)
            .Build();
    }

    private void Update()
    {
        effectTimer.Tick(Time.deltaTime);
    }

    private void EffectEnded()
    {
        UnityActionUtility.Invoke(finished);
    }

    private void LerpPosition()
    {
        float percentage = effectTimer.time / effectTimer.interval;
        transform.position = Vector3.Lerp(initialPosition, target.position, percentage);
    }

    public void PickTo(Transform destination)
    {
        target = destination;
        initialPosition = transform.position;

        // setting isKinematic to `true` results in rigidbodies above the picked one freeze 
        // mid-air until next unrelated collision wakes them up. manually replicating behaviour of 
        // a kinematic body avoids that.
        rigidbody.constraints = RigidbodyConstraints.FreezeAll;
        rigidbody.useGravity = false;

        // no need to disable Pickable component as disabling Collider will ensure object 
        // currently being picked does not get hit by PlayerPicking raycasts
        collider.enabled = false;

        effectTimer.Start();
    }
}
