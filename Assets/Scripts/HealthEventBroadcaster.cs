﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class HealthEvents
{
    public const string Initialized = "health.initialized";
    public const string HpChanged = "health.hpChanged";
    public const string HpDecreased = "health.hpDecreased";
    public const string ZeroHpReached = "health.zeroHpReached";
}

public class HealthEventData
{
    private int m_HpDelta;
    public int hpDelta { get { return m_HpDelta; } }

    private int m_HpAbsDelta;
    public int hpAbsDelta { get { return m_HpAbsDelta; } }

    private int m_Hp;
    public int hp { get { return m_Hp; } }

    private int m_MaxHp;
    public int maxHp { get { return m_MaxHp; } }

    public HealthEventData(int hp, int maxHp)
    {
        m_HpDelta = 0;
        m_HpAbsDelta = 0;

        m_Hp = hp;
        m_MaxHp = maxHp;
    }

    public HealthEventData(int hpDelta, int hp, int maxHp)
    {
        m_HpDelta = hpDelta;
        m_HpAbsDelta = Mathf.Abs(m_HpDelta);

        m_Hp = hp;
        m_MaxHp = maxHp;
    }
}

public class HealthEventBroadcaster : MonoBehaviour
{
    [SerializeField]
    private Health health;

    [SerializeField]
    private Thing thing;

    private void OnEnable()
    {
        health.hpChanged += OnHpChanged;
        health.hpDecreased += OnHpDecreased;
        health.zeroHpReached += OnZeroHpReached;
    }

    private void Start()
    {
        EventManager.Invoke(
            EventUtility.TypedEvent(HealthEvents.Initialized, thing.type), 
            new HealthEventData(health.healthPoints, health.maxHealthPoints));
    }

    private void OnDisable()
    {
        health.hpChanged -= OnHpChanged;
        health.hpDecreased -= OnHpDecreased;
        health.zeroHpReached -= OnZeroHpReached;
    }

    private void OnHpChanged(int hpDelta)
    {
        EventManager.Invoke(
            EventUtility.TypedEvent(HealthEvents.HpChanged, thing.type),
            new HealthEventData(hpDelta, health.healthPoints, health.maxHealthPoints));
    }

    private void OnHpDecreased(int hpDecrement)
    {
        EventManager.Invoke(
            EventUtility.TypedEvent(HealthEvents.HpDecreased, thing.type), 
            new HealthEventData(-hpDecrement, health.healthPoints, health.maxHealthPoints));
    }

    private void OnZeroHpReached()
    {
        EventManager.Invoke(
            EventUtility.TypedEvent(HealthEvents.ZeroHpReached, thing.type));
    }
}
