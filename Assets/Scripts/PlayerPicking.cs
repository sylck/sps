﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPicking : MonoBehaviour
{
    [SerializeField]
    private PlayerInventory inventory;
    
    [SerializeField]
    private Camera playerCamera;

    [SerializeField]
    private Transform playerTransform;

    [SerializeField]
    private float maxPickingDistance = 3f;

    [SerializeField]
    private LayerMask projectileLayer;

    [SerializeField]
    private AudioSource audioSource;

    [SerializeField]
    private AudioClip failSound,
                      pickSound;

    [SerializeField]
    private float minFailSoundInterval = .2f;

    private Pickable lastPickable;
    private Timer failSoundDelayTimer;

    private struct PickableScanResult
    {
        public bool found;
        public bool different;
        public Pickable pickable;
    }

    private void Start()
    {
        failSoundDelayTimer = new Timer.Builder()
            .SetInterval(minFailSoundInterval)
            .SetAutoReset(false)
            .Build();
    }

    // Update is called once per frame
    void Update()
    {
        failSoundDelayTimer.Tick(Time.deltaTime);
        
        PickableScanResult pickable = ScanForPickable();

        // if cursor moved from a pickable to nothing or another pickable, reset previously 
        // highlighted one
        if (lastPickable != null && pickable.different)
            ResetLastPickable();

        if (pickable.found && pickable.different)
            ActivateNewPickable(pickable.pickable);

        // skip one cycle if pickable is found but different to the previously highlighted one to 
        // let the player digest screen changes before proceeding with further picking
        if (pickable.found && !pickable.different && GetPickInput())
            TryPick();
    }

    private PickableScanResult ScanForPickable()
    {
        PickableScanResult result = new PickableScanResult();

        // recalculating screen center in every call because Unity does not emit an event on 
        // resolution change. a manual approach would rely on polling, so no improvements, at 
        // least as long as this is the only one piece of code which checks it
        Vector3 screenCenter = new Vector3(Screen.width / 2, Screen.height / 2);

        RaycastHit raycastHit;
        Ray ray = playerCamera.ScreenPointToRay(screenCenter);
        Physics.Raycast(ray, out raycastHit, maxPickingDistance, 
            Physics.DefaultRaycastLayers & ~projectileLayer.value, QueryTriggerInteraction.Ignore);

        Transform hitTransform = raycastHit.transform;

        if (hitTransform != null)
            result.pickable = hitTransform.GetComponent<Pickable>();

        result.found = result.pickable != null;
        result.different = result.pickable != lastPickable;

        return result;
    }

    private bool GetPickInput()
    {
        return Input.GetKey(KeyCode.E);
    }

    private void TryPlayFailSound()
    {
        if (failSoundDelayTimer.enabled)
            return;

        audioSource.PlayOneShot(failSound);
        failSoundDelayTimer.Start();
    }

    private void PlayPickSound()
    {
        audioSource.PlayOneShot(pickSound);
    }

    private void TryPick()
    {
        Thing thing = lastPickable.GetComponent<Thing>();
        ItemData itemData = lastPickable.itemData;

        if (inventory.CanAddItem(thing.type, itemData))
        {
            Pick();
            PlayPickSound();
        }
        else
        {
            Debug.Log("Cannot pick '" + itemData.displayName + "', inventory is full.");
            TryPlayFailSound();
        }
    }

    private void Pick()
    {
        lastPickable.picked += OnPickablePicked;
        lastPickable.Pick(playerTransform);
        ResetLastPickable();
    }

    private void OnPickablePicked(Pickable pickable)
    {
        Thing thing = pickable.GetComponent<Thing>();
        inventory.AddItem(thing.type, pickable.itemData);
    }

    private void ResetLastPickable()
    {
        lastPickable.Deactivate();

        lastPickable = null;
    }

    private void ActivateNewPickable(Pickable newPickable)
    {
        newPickable.Activate();

        lastPickable = newPickable;
    }
}
