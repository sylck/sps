﻿using System.Collections.Generic;
using UnityEngine;

public static class PlayerInventoryEvents
{
    public const string ItemCountInitialized = "playerInventory.itemCountInitialized";
    public const string ItemCountChanged = "playerInventory.itemCountChanged";

    public const string GridInitialized = "playerInventory.gridInitialized";
}

public class PlayerInventory : MonoBehaviour
{
    private Inventory.Stats stats;
    private Inventory.Grid grid;

    private void Start()
    {
        stats = new Inventory.Stats();
        foreach (KeyValuePair<ThingType, int> stat in stats.stats)
            EventManager.Invoke(
                EventUtility.TypedEvent(PlayerInventoryEvents.ItemCountInitialized, stat.Key),
                stat.Value);

        grid = new Inventory.Grid(OnStackDestroyed);
        EventManager.Invoke(PlayerInventoryEvents.GridInitialized, grid);
    }

    private void OnStackDestroyed(Inventory.Stack destroyedStack)
    {
        stats.ApplyChange(destroyedStack.itemType, -destroyedStack.itemCount);
        BroadcastItemCountChanged(destroyedStack.itemType, stats.GetItemCount(destroyedStack.itemType));
    }

    public bool CanAddItem(ThingType item, ItemData itemData)
    {
        return CanAddItems(item, itemData, 1);
    }

    private bool CanAddItems(ThingType item, ItemData itemData, int count)
    {
        return grid.CheckCanAddItems(item, count, itemData);
    }

    public bool AddItem(ThingType item, ItemData itemData)
    {
        return AddItems(item, itemData, 1);
    }

    private bool AddItems(ThingType item, ItemData itemData, int count)
    {
        bool success = grid.TryAddItems(item, count, itemData);

        if (success)
        {
            stats.ApplyChange(item, count);
            BroadcastItemCountChanged(item, stats.GetItemCount(item));
        }

        return success;
    }

    public bool RemoveItem(ThingType item)
    {
        return RemoveItems(item, 1);
    }

    private bool RemoveItems(ThingType item, int count)
    {
        bool success = grid.TryRemoveItems(item, count);

        if (success)
        {
            stats.ApplyChange(item, -count);
            BroadcastItemCountChanged(item, stats.GetItemCount(item));
        }

        return success;
    }

    private void BroadcastItemCountChanged(ThingType item, int count)
    {
        EventManager.Invoke(
            EventUtility.TypedEvent(PlayerInventoryEvents.ItemCountChanged, item),
            count);
    }
}
