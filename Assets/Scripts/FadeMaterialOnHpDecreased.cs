﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeMaterialOnHpDecreased : MonoBehaviour
{
    [SerializeField]
    private Health health;

    [SerializeField]
    private MeshRenderer meshRenderer;

    [SerializeField]
    private Material normalMaterial, damagedMaterial;

    private Material material;

    private void OnEnable()
    {
        health.hpDecreased += OnHpDecreased;
    }

    private void Start()
    {
        material = meshRenderer.material;
    }

    private void OnDisable()
    {
        health.hpDecreased -= OnHpDecreased;
    }

    private void OnHpDecreased(int hpDelta)
    {
        float hpLoss = 
            (float)(health.maxHealthPoints - health.healthPoints) / health.maxHealthPoints;

        material.Lerp(normalMaterial, damagedMaterial, hpLoss);
    }
}
