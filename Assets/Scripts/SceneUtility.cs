﻿using System;
using UnityEngine.SceneManagement;

public class SceneUtility
{
    public const int UndefinedSceneBuildIndex = -1;

    public enum SceneType
    {
        Undefined = UndefinedSceneBuildIndex,

        Startup = 0,
        Common = 1,

        MainMenu = 2,
        Level = 3
    }
    
    public static void LoadScene(SceneType scene, LoadSceneMode mode = LoadSceneMode.Single)
    {
        SceneManager.LoadScene((int)scene, mode);
    }

    public static SceneType SceneBuildIndexToType(int buildIndex)
    {
        if (buildIndex >= (int)SceneType.Level)
            return SceneType.Level;
        else
            return (SceneType)buildIndex;
    }

    public static bool SceneBuildIndexIsType(int buildIndex, SceneType type)
    {
        return SceneBuildIndexToType(buildIndex) == type;
    }

    public static SceneType ActiveSceneType()
    {
        return SceneBuildIndexToType(SceneManager.GetActiveScene().buildIndex);
    }

    public static void ReloadActiveScene()
    {
        SceneManager.LoadScene(
            SceneManager.GetActiveScene().buildIndex);
    }
}
