﻿using UnityEngine;

public class GameObjectUtility
{
    public static void ToggleVisibility(GameObject gameObject)
    {
        gameObject.SetActive(!gameObject.activeSelf);
    }

    public static bool Visible(GameObject gameObject)
    {
        return gameObject.activeInHierarchy;
    }

    public static void Show(GameObject gameObject)
    {
        gameObject.SetActive(true);
    }

    public static void Show(GameObject[] gameObjects)
    {
        SetActive(gameObjects, true);
    }

    public static void Hide(GameObject gameObject)
    {
        gameObject.SetActive(false);
    }

    public static void Hide(GameObject[] gameObjects)
    {
        SetActive(gameObjects, false);
    }

    public static void SetActive(GameObject[] gameObjects, bool value)
    {
        foreach (GameObject gameObject in gameObjects)
        {
            gameObject.SetActive(value);
        }
    }
}
