﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    [SerializeField]
    private PoolMember poolMember;
    
    [SerializeField]
    private GameObject explosionPrefab;
    
    [SerializeField]
    private float maxLifeTime = 2f,
                  explosionRadius = .3f,
                  explosionForceThreshold = 80f;

    [SerializeField]
    private int hpDamage = 5;
    
    private Timer activeTimer;
    private bool queuedForDestruction;

    private const DamageType damageType = DamageType.Projectile;

    private void Awake()
    {
        activeTimer = new Timer.Builder()
            .SetInterval(maxLifeTime)
            .SetAutoReset(false)
            .AddElapsedListener(Explode)
            .Build();   
    }

    private void OnEnable()
    {
        // this is a pooled object, so reset queuedForDestruction to false and restart 
        // activeTimer each time object is activated, i.e. brought back from pool to scene
        queuedForDestruction = false;

        activeTimer.Start();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (queuedForDestruction)
            return;

        Vector3 totalForce = collision.impulse / Time.fixedDeltaTime;

        if (totalForce.magnitude > explosionForceThreshold)
            Explode();
    }

    // Update is called once per frame
    void Update()
    {
        // destroy self if below ground
        if (transform.position.y < -1f)
        {
            DestroySelf();
            return;
        }

        activeTimer.Tick(Time.deltaTime);
    }

    private void OnDisable()
    {
        // ... and stop timer when returning to pool
        activeTimer.Stop();
    }

    private void Explode()
    {
        Vector3 explosionPosition = gameObject.transform.position;

        Collider[] hits = Physics.OverlapSphere(explosionPosition, explosionRadius);
        foreach (Collider hit in hits)
        {
            Damageable damageable = hit.GetComponent<Damageable>();

            // don't request to take damage if component disabled (as is the case when object is 
            // pending destruction)
            if (damageable != null && damageable.enabled && damageable.SusceptibleTo(damageType))
                damageable.TakeDamage(damageType, hpDamage);
        }

        // explosion effect
        GameObject explosion = Instantiate(explosionPrefab);
        explosion.transform.position = explosionPosition;

        DestroySelf();
    }

    private void DestroySelf()
    {
        queuedForDestruction = true;
        poolMember.ReturnToPool();
    }
}
