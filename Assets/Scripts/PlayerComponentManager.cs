﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerComponentManager : MonoBehaviour
{
    [SerializeField]
    private GameManager gameManager;

    [SerializeField]
    private PlayerMovement playerMovement;

    [SerializeField]
    private PlayerPicking playerPicking;

    [SerializeField]
    private PlayerShooting playerShooting;

    private void OnEnable()
    {
        EventManager.AddListener(
            GameManagerEvents.GameWillStartRunning,
            EnableComponentsHandler);
        EventManager.AddListener(
            GameManagerEvents.GameWillStopRunning,
            DisableComponentsHandler);

        // once GameManager reaches a final state, it will no longer emit events or affect time 
        // flow, so listen for when Menu shows so that components can be paused independently of 
        // events emitted by GameManager. listening for MenuEvents.Hidden not necessary as 
        // GameManagerEvents.GameWillStopRunning covers relevant UI element hide events and once 
        // final state is reached, under no condition should the components be re-enabled.
        EventManager.AddListener(
            UI.MenuEvents.Showing,
            DisableComponentsHandler);

        EventManager.AddListener(
            Inventory.UI.InventoryDialogEvents.Showing,
            DisableComponentsHandler);
        EventManager.AddListener(
            Inventory.UI.InventoryDialogEvents.Hidden,
            TryEnableComponentsHandler);
    }

    private void OnDisable()
    {
        EventManager.RemoveListener(
            GameManagerEvents.GameWillStartRunning,
            EnableComponentsHandler);
        EventManager.RemoveListener(
            GameManagerEvents.GameWillStopRunning,
            DisableComponentsHandler);

        EventManager.RemoveListener(
            UI.MenuEvents.Showing,
            DisableComponentsHandler);

        EventManager.RemoveListener(
            Inventory.UI.InventoryDialogEvents.Showing,
            DisableComponentsHandler);
        EventManager.RemoveListener(
            Inventory.UI.InventoryDialogEvents.Hidden,
            TryEnableComponentsHandler);
    }

    private void SetComponentsState(bool state)
    {
        playerMovement.enabled = state;
        playerPicking.enabled = state;
        playerShooting.enabled = state;
    }

    private void DisableComponents()
    {
        SetComponentsState(false);
    }

    private void TryEnableComponents()
    {
        if (!gameManager.running || gameManager.enteredFinalState)
            return;

        EnableComponents();
    }

    private void EnableComponents()
    {
        SetComponentsState(true);
    }

    private void DisableComponentsHandler(EventData data)
    {
        DisableComponents();
    }

    private void EnableComponentsHandler(EventData data)
    {
        EnableComponents();
    }

    private void TryEnableComponentsHandler(EventData data)
    {
        TryEnableComponents();
    }
}
