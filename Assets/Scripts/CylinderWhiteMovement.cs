﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CylinderWhiteMovement : MonoBehaviour
{
    [SerializeField]
    private Transform cylinderWhite;

    [SerializeField]
    private NavMeshAgent navMeshAgent;

    [SerializeField]
    private float maxDestinationDistance = 2f;

    [SerializeField]
    private MovementSoundManager movementSound;

    private Vector3[] patrolPoints;
    private Vector3 destinationPosition;
    private int currentPpIndex;
    private int maxPpIndex;
    private float maxDestinationSqrDistance;

    private const int emptyMaxPpIndex = -1;  // maxPpIndex value when patrolPoints is empty

    public void SetPatrolPoints(Vector3[] points)
    {
        patrolPoints = points;

        maxPpIndex = patrolPoints.Length - 1;

        // stick to initial position if patrolPoints empty
        if (maxPpIndex == emptyMaxPpIndex)
        {
            destinationPosition = cylinderWhite.position;
        }
        else
        {
            currentPpIndex = -1;
        }
    }

    private void Start()
    {
        movementSound.SetMaxVelocityMagnitude(navMeshAgent.speed);

        maxDestinationSqrDistance = Mathf.Pow(maxDestinationDistance, 2);
        SetNextDestination();
    }

    // Update is called once per frame
    void Update()
    {
        UpdateDestinationIfReached();
        UpdateMovementSound();
    }

    private void UpdateDestinationIfReached()
    {
        if (!CheckDestinationReached())
            return;

        SetNextDestination();
    }

    private void UpdateMovementSound()
    {
        movementSound.SetVelocity(navMeshAgent.velocity);
    }

    private bool CheckDestinationReached()
    {
        float destinationSqrDistance =
            (destinationPosition - cylinderWhite.position).sqrMagnitude;
        return destinationSqrDistance < maxDestinationSqrDistance;
    }

    private void SetNextDestination()
    {
        // do nothing if patrolPoints is empty
        if (maxPpIndex == emptyMaxPpIndex)
            return;

        int nextPpIndex = currentPpIndex + 1;

        if (nextPpIndex > maxPpIndex)
            nextPpIndex = 0;

        destinationPosition = patrolPoints[nextPpIndex];
        currentPpIndex = nextPpIndex;

        navMeshAgent.SetDestination(destinationPosition);
    }
}
